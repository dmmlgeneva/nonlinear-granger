function P = l21Prox(A,alp,pos)
% L21PROX - a utility function to calculate l_2/1 proximal on columns of matrix
%
% CREATED: MG - 12/09/2016

%% fill in optional arguments
if ~exist('pos','var') || isempty(pos),
  pos = 0;
end

numCols = size(A,2);

P = zeros(size(A));
if pos==1 % if A shall have only positive elements
  for i = 1:numCols
    P(:,i) = max(A(:,i) * max((1- alp/norm(A(:,i),2)),0),0);
  end  
else
  for i = 1:numCols
    P(:,i) = A(:,i) * max((1- alp/norm(A(:,i),2)),0);
  end
end

end