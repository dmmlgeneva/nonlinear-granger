function [K,Phi] = getKernel(kType,xTrain,xOut)
% getKernel - get gram matrix for 
%
% INPUTS
%   xTrain - n1 x m matrix with training features
%   xOut - n2 x m matrix with features for the prediction dataset (train or test)
%   kType - kernel type, see getKernel.m for possible values
%
% OUTPUTS
%     K - n1 x n2 gram matrix with elements K(i,j) = k(xTrain_i,xOut_j)
%     
% EXAMPLE: K = getKernel(xTrain,xOut)
%
% CREATED: MG - 19/07/2016
% MODIFICATION HISTORY:
%   MG - 02/09/2016: added kernel normalization
%   MG - 02/09/2016: made xOut optional (may be same as xTrain)
%   MG - 03/09/2016: changed kernel normalization to simply have the same trace (multiplicative normalization Kloft2011)
%   MG - 12/09/2016: added kernel eigendecomposition

%% fill in optional arguments
if ~exist('xOut','var') || isempty(xOut),
  xOut = xTrain;
  sameX = 1;
else 
  sameX = 0;
end

%%% this was used in xme, xln, rcn, rcl
%% define kernel function
if kType == 1 % linear kernel
  kFun = @(x,y) x*y'; 
elseif kType == 2 % gaussian kernel with sigma^2 = 1
  kFun = @(x,y) exp(-0.5*sumSquared(x-y));
elseif kType == 3 % gaussian kernel with sigma^2 = 2
  kFun = @(x,y) exp(-0.25*sumSquared(x-y));
elseif kType == 4 % gaussian kernel with sigma^2 = 0.5
  kFun = @(x,y) exp(-sumSquared(x-y));
elseif kType == 5 % quadratic kernel
  kFun = @(x,y) (x*y'+1)^2;
elseif kType == 6 % cubic kernel
  kFun = @(x,y) (x*y'+1)^3;
end


% %%% this was used in xge
% %% define kernel function - the are all gaussians with different width
% if kType == 1 
%   kFun = @(x,y) exp(-0.1*sumSquared(x-y));
% elseif kType == 2
%   kFun = @(x,y) exp(-0.25*sumSquared(x-y));
% elseif kType == 3
%   kFun = @(x,y) exp(-0.5*sumSquared(x-y));
% elseif kType == 4
%   kFun = @(x,y) exp(-sumSquared(x-y));
% elseif kType == 5
%   kFun = @(x,y) exp(-2*sumSquared(x-y));
% elseif kType == 6
%   kFun = @(x,y) exp(-4*sumSquared(x-y));
% end


%% calculate the gram matrix
n1 = size(xTrain,1);
n2 = size(xOut,1);
% first do the diagonals
dKTrain = zeros(n1,1);
for i=1:n1
  dKTrain(i) = kFun(xTrain(i,:),xTrain(i,:));
end
scaling = n1/sum(dKTrain);
  
K = zeros(n1,n2);
if sameX == 1 % will be symmetrical so only need upper triangular
  for i = 1:n1
    for j = i:n2
      K(i,j) = kFun(xTrain(i,:),xOut(j,:))*scaling; % normalize as in TechNotes section 4.1
    end
  end
  K = K+K'-diag(diag(K)); % get the lower triangular and compensate for doubling the diagonal
else
  for i = 1:n1
    for j=1:n2
      K(i,j) = kFun(xTrain(i,:),xOut(j,:))*scaling;
    end
  end
end

%% do eigen decomposition on training gram matrix K=Phi*Phi'
if sameX == 1
  [V,D] = eig(K);
  Phi = real(V*sqrt(D)); % only the real part since due to calc precision there may be some tiny negative eigenvalues
end


end