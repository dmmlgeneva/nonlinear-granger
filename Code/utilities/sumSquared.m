function c = sumSquared(a)
% SUMSQAURED a utility function norm(a)^2
%
% CREATED: MG - 19/07/2016

c = sum(sum(a.*a));

end