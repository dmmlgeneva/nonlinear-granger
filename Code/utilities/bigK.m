function bK = bigK(Ks,gs,kFiles,isTest)
% BIGK - a utility function wighting multiple kernels by gammas, used in MKL
%
% CREATED: MG - 10/09/2016

if ~isempty(Ks)
  gK = cellfun(@(x,y) x*y,Ks,num2cell(gs),'UniformOutput',0);
  bK=0;
  for i = 1:numel(gK);
    bK = bK + gK{i}; % sum the cell elements with matrices
  end
elseif ~isTest
  bK=0;
  for i = 1:numel(gs);
    bK = bK + kFiles{i}.K*gs(i); % sum the cell elements with matrices
  end
else
  bK=0;
  for i = 1:numel(gs);
    bK = bK + kFiles{i}.KTest*gs(i); % sum the cell elements with matrices
  end
end

end
