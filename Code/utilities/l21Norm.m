function n = l21Norm(A)
% L21NORM - a utility function to calculate l_2/1 norm on columns of matrix
%
% CREATED: MG - 12/09/2016

numCols = size(A,2);

n = 0;
for i = 1:numCols
  n = n + norm(A(:,i),2);
end

end