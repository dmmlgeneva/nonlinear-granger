function [mse,resid] = getError(trueOut,predOut)
% GETERROR - calculate prediction error (mean squared error)
%
% INPUTS
%   trueOut - n x m matrix with true output data
%   predOut - n x m matrix with predicted output data
%
% OUTPUTS
%     resid - n x m matrix with precition resiudals
%     mse - m vector with prediction mean squared error per series
%     
% EXAMPLE: mse = getError(true,pred)
%
% CREATED: MG - 16/07/2016

resid = trueOut - predOut;
mse = sum(resid.*resid,1)/size(trueOut,1);

end