function fitMKLE(dataFolder,dataFile,kTypeList,recalculateKernels,lbdaGrid,inMemory)
% FITMKLE - fit MKL model for the full regularization path using eigendecomposition algo
%   optimised for large number of large kernels
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
%   kTypeList - list of kernel types, see getKernel.m for possible values (Default = [1:6])
%   lbdaGrid - cell with lambda grid
%   recalculateKernels - recalculate kernels (if not sure that the stored ones are still correct, default = 0)
%   inMemory - switch for processing the kernels in memory or by I/Os
% 
% EXAMPLE: fitMKLES('N_ma','zC5_30_500',1)
%
% CREATED: MG - 13/09/2016

%% fill in optional arguments
if ~exist('kTypeList','var') || isempty(kTypeList),
  kTypeList = [1:6];
end
if ~exist('recalculateKernels','var') || isempty(recalculateKernels),
  recalculateKernels = 0;
end
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = num2cell(logspace(-3,4,15));
end
if ~exist('inMemory','var') || isempty(inMemory),
  inMemory = 1;
end


%% load data
tic
numKs = 0; % total number of kernels;
kFiles = cell(length(kTypeList),1);
for kType = kTypeList
  numKs = numKs+1;
  %% load or calculate the kernels for all input data
  if recalculateKernels ~= 1 && exist(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat'], 'file') == 2
    kFiles{numKs} = matfile(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat']);
  else
    mFile = matfile(['expData/',dataFolder,'/',dataFile,'.mat']);
    [K,Phi] = getKernel(kType,mFile.xTrain);
    KTest = getKernel(kType,mFile.xTrain,mFile.xTest);
    save(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat'],'K','KTest','Phi')
    kFiles{numKs} = matfile(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat']);
  end
end
%% decide weather do inMemory processing or I/O processing
sizeK = size(kFiles{numKs}.K,1);
if ~inMemory==1 || sizeK^2*numKs >= 2e8
  inMemory = 0;
  PhiCell = cellfun(@(x) x.Phi,kFiles,'UniformOutput',0);
else
  PhiCell = cellfun(@(x) x.Phi,kFiles,'UniformOutput',0);
  KCell = cellfun(@(x) x.K,kFiles,'UniformOutput',0);
  KTestCell = cellfun(@(x) x.KTest,kFiles,'UniformOutput',0);
end

%% load the output yTrain
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain')

% prepare for saving data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)

%% get the whole regularization path
trainLength = size(yTrain,1);
gamma = ones(numKs,1)/numKs; % initiate evenly
cVec = zeros(trainLength,1);
% adapt lambda grid to the size of the problem
lbdaGrid = cellfun(@(x) x*sqrt(trainLength)*numKs,lbdaGrid,'UniformOutput',0);
% go from largest lambda to warm start gamma
for lbdIdx = [length(lbdaGrid):-1:1]
  lbda = lbdaGrid{lbdIdx};  
  %% eigen algo 
  if inMemory
    [cVec,gamma,objTotal] = MKLEigenAlgoSmall(yTrain,PhiCell,cVec,gamma,lbda);
    Theta = cVec;
    predTrain = bigK(KCell,gamma)'*Theta;
    predTest = bigK(KTestCell,gamma)'*Theta; % note the transpose here!
  else
    %[cVec,gamma,objTotal] = MKLEigenAlgoFast(yTrain,kFiles,phiFiles,cVec,gamma,lbda);
    %[cVec,gamma,objTotal] = MKLEigenAlgo(yTrain,kFiles,cVec,gamma,lbda);
    [cVec,gamma,objTotal] = MKLEigenAlgoSmall(yTrain,PhiCell,cVec,gamma,lbda);
    Theta = cVec;
    predTrain = bigK([],gamma,kFiles,0)'*Theta;
    predTest = bigK([],gamma,kFiles,1)'*Theta; % note the transpose here!
  end
  
  % save the results
  save(['expResults/',dataFolder,'/',dataFile,'/Result_MKLE','_',num2str(lbdIdx,'%02.0f'),'.mat'],'Theta','predTrain','predTest','lbda','gamma','kTypeList','objTotal','inMemory')
  fprintf('fitMKLE %s inMemory:%d \n',['expResults/',dataFolder,'/',dataFile,'/Result_MKLE','_',num2str(lbdIdx,'%02.0f'),'.mat'],inMemory)
  
end

end
