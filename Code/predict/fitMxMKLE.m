function fitMxMKLE(dataFolder,dataFoldersIn,dataFile,kTypeList,recalculateKernels,lbdaGrid,inMemory)
% FITMXMKLE - fit multivariate MK model for the full regularization path using eigendecomposition algo
%   use single kernel for combination of inputs (unlike MvMKLE which has a kernel per input)
%   output is a single series, inputs are multiple series in the In list
%
% INPUTS
%   dataFolder - name of the data folder with the output series (will search for it in the expData folder)
%   dataFoldersIn - cell with names of the data folder with the input series (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
%   kTypeList - list of kernel types, see getKernel.m for possible values (Default = [1:6])
%   lbdaGrid - cell with lambda grid
%   recalculateKernels - recalculate kernels (if not sure that the stored ones are still correct, default = 0)
% 
% EXAMPLE: fitMxMKLE('N_ma','zC5_30_500',1)
%
% CREATED: MG - 27/9/2016

%% fill in optional arguments
if ~exist('kTypeList','var') || isempty(kTypeList),
  kTypeList = [1:6];
end
if ~exist('recalculateKernels','var') || isempty(recalculateKernels),
  recalculateKernels = 0;
end
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = num2cell(logspace(-3,4,15));
end
if ~exist('inMemory','var') || isempty(inMemory),
  inMemory = 1;
end

%% prepare list of files (for conveniece, include the out set in the list of in sets)
dataFoldersIn = sort(unique([dataFoldersIn {dataFolder}]));

%% load data
numIn = numel(dataFoldersIn); % number of input series
numKs = 0; % total number of kernels;
kFiles = cell(length(kTypeList),1);
xTrainMv=[]; xTestMv=[];
for kType = kTypeList
  numKs = numKs+1;
  %% load or calculate the kernels for all input data
  if recalculateKernels ~= 1 && exist(['expData/',dataFoldersIn{1},'/',dataFile,'_xK',num2str(kType),'.mat'], 'file') == 2
    kFiles{numKs} = matfile(['expData/',dataFoldersIn{1},'/',dataFile,'_xK',num2str(kType),'.mat']);
  else
    if size(xTrainMv,1) == 0
    % merge all the inputs
      for inIdx = 1:numIn
        dataF = dataFoldersIn{inIdx};
        mFile = matfile(['expData/',dataF,'/',dataFile,'.mat']);
        numLags = size(mFile.xTrain,2);
        idxs = (inIdx-1)*numLags+(1:numLags);
        xTrainMv(:,idxs) = mFile.xTrain;
        xTestMv(:,idxs) = mFile.xTest;
      end
    end
    % calculate the kernels
    [K,Phi] = getKernel(kType,xTrainMv);
    KTest = getKernel(kType,xTrainMv,xTestMv);
    % and save (always just the the 1st series in the group)
    save(['expData/',dataFoldersIn{1},'/',dataFile,'_xK',num2str(kType),'.mat'],'K','KTest','Phi')
    kFiles{numKs} = matfile(['expData/',dataFoldersIn{1},'/',dataFile,'_xK',num2str(kType),'.mat']);
  end
end

%% load the output yTrain, yTest
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain')

%% decide weather do inMemory processing or I/O processing
sizeK = size(kFiles{numKs}.K,1);
if ~inMemory==1 || sizeK^2*numKs >= 2e8
  inMemory = 0;
  PhiCell = cellfun(@(x) x.Phi,kFiles,'UniformOutput',0);
else
  PhiCell = cellfun(@(x) x.Phi,kFiles,'UniformOutput',0);
  KCell = cellfun(@(x) x.K,kFiles,'UniformOutput',0);
  KTestCell = cellfun(@(x) x.KTest,kFiles,'UniformOutput',0);
end
fprintf('Will work inMemory:%d \n',inMemory);

% prepare for saving data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)

%% get the whole regularization path
trainLength = size(yTrain,1);
gamma = ones(numKs,1)/numKs; % initiate evenly
cVec = zeros(trainLength,1);
% adapt lambda grid to the size of the problem
lbdaGrid = cellfun(@(x) x*sqrt(trainLength)*numKs,lbdaGrid,'UniformOutput',0);
% go from largest lambda to warm start gamma
for lbdIdx = [length(lbdaGrid):-1:1]
  lbda = lbdaGrid{lbdIdx};  
  %% eigenalgo
  if inMemory
    [cVec,gamma,objTotal] = MKLEigenAlgoSmall(yTrain,PhiCell,cVec,gamma,lbda);
    Theta = cVec;
    predTrain = bigK(KCell,gamma)'*Theta;
    predTest = bigK(KTestCell,gamma)'*Theta; % note the transpose here!
  else
    %[cVec,gamma,objTotal] = MKLEigenAlgo(yTrain,kFiles,cVec,gamma,lbda);
    %[cVec,gamma,objTotal] = MKLEigenAlgoFast(yTy,PhiTy,kFiles,PTPFileName,cVec,gamma,lbda);
    [cVec,gamma,objTotal] = MKLEigenAlgoSmall(yTrain,PhiCell,cVec,gamma,lbda);
    Theta = cVec;
    predTrain = bigK([],gamma,kFiles,0)'*Theta;
    predTest = bigK([],gamma,kFiles,1)'*Theta; % note the transpose here!
  end
   
  % save results
  save(['expResults/',dataFolder,'/',dataFile,'/Result_MxMKLE_',num2str(lbdIdx,'%02.0f'),'.mat'],'Theta','predTrain','predTest','lbda','gamma','kTypeList','dataFoldersIn','objTotal')
  fprintf('fitMxMKLE %s inMemory:%d \n',['expResults/',dataFolder,'/',dataFile,'/Result_MxMKLE_',num2str(lbdIdx,'%02.0f'),'.mat'],inMemory)
  
end
% and clean the temp file
% delete(['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLE_Temp.mat'])


end
