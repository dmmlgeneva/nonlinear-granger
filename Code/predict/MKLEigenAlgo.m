function [cVec,gamma,objTotal] = MKLEigenAlgo(yTrain,kFiles,cVec,gamma,lbda,updateTresh,maxIter)
% MKLEIGENALGO - eigendecomposition algo for MKL optimised for large number of large kernels
%
% INPUTS
%   yTrain - output data
%   kFiles - cell with matfiles containing the kernels
%   cVec, gamma - initial values of the MKL function
%   lbda - lambda hyper parameter
%   updateTresh - if change in objective smaller than this, stop grad descent (more compliated, check code)
%   maxIter - max number of iterations
%
% OUTPUTS
%   cVec, gamma - parameters of the MKL functions
% 
% EXAMPLE: MKLEigenAlgo('N_ma','zC5_30_500',1)
%
% CREATED: MG - 13/09/2016
% MODIFICATION HISTORY:
%   MG - 04/09/2016: corrected the final cVec calculation (and removed from inputs)

%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-03;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 1000;
end

%% group lasso for the feature representation
numGroups = length(gamma);
zMat = zeros(length(cVec),numGroups); % vectors z are organised into a matrix
for gIdx = 1:numGroups
  zMat(:,gIdx) = gamma(gIdx)*kFiles{gIdx}.Phi'*cVec;
end
%% by proximal gradient descent
function a = getPhiZ(zM)
  a = 0;
  for i = 1:numGroups
    a = a + kFiles{i}.Phi*zM(:,i);
  end
end
% function definitions
fFunc = @(yMphiZTemp) sumSquared(yMphiZTemp);
gradFunc = @(yMphiZTemp,idx) -2*kFiles{idx}.Phi'*yMphiZTemp;
proxFunc = @(zTemp,alp) l21Prox(zTemp,2*sqrt(lbda)*alp);
%objFunc = @(yMphiZTemp) fFunc(yMphiZTemp) + 2*sqrt(lbda)*l21Norm(zTemp); 
% step size parameters
alpha = 100; beta = 0.5; 
% initiate func values;
gradF = zeros(length(cVec),numGroups);
yMphiZ = yTrain-getPhiZ(zMat); % (y - Phi*z)
oldF = fFunc(yMphiZ);
% initiate grad descent moitoring
objTotal = 0;
objTotal(1,1) = oldF+2*sqrt(lbda)*l21Norm(zMat);
while 1
  % evaluate grad
  for gIdx = 1:numGroups
    gradF(:,gIdx) = gradFunc(yMphiZ,gIdx);
  end
  % gradient step with ISTA line search (BackTaboulle2010)
  while alpha>1e-5
    zNew = proxFunc(zMat - alpha*gradF,alpha);
    if norm(zNew-zMat) < 1e-04
      alpha = alpha*beta; newF = oldF; yMphiZNew = yMphiZ;
      continue;
    end
    yMphiZNew = yTrain-getPhiZ(zNew); % (y - Phi*z)
    newF = fFunc(yMphiZNew);
    if newF <= oldF + [gradF(:)]'*(zNew(:)-zMat(:)) + 0.5/alpha*sumSquared(zNew(:)-zMat(:))
      break
    else
      alpha = alpha*beta;
    end
  end
  % update step variables;
  zMat = zNew; yMphiZ = yMphiZNew; oldF = newF;
  % check convergence of prox grad descent 
  objTotal = [objTotal; oldF+2*sqrt(lbda)*l21Norm(zMat)];
  if length(objTotal)>5 && sum(objTotal(end-5:end-1)-objTotal(end)) < updateTresh
    break
  elseif length(objTotal)>maxIter % or check maxIter
    break
  end
end

%% recover gamma and solve linear system for cVec
sumK = 0;
sumPhiZ = 0;
for j=1:numGroups
  gamma(j) = sqrt(lbda)*norm(zMat(:,j),2);
  sumK = sumK + gamma(j)^2*kFiles{j}.K;
  sumPhiZ = sumPhiZ + gamma(j)*kFiles{j}.Phi*zMat(:,j);
end
cVec = sumK \ sumPhiZ;
cVec(isnan(cVec))=0;
%% solve linear system for cVec
% zMat = bsxfun(@rdivide,zMat,gamma');
% % replace nans by zeros
% zMat(isnan(zMat))=0;
% cVec = sumK \ getPhiZ(zMat);

% %%%%% testing
% sumK=0
% for i=1:numGroups
%   sumK = sumK + gamma(i)*kFiles{i}.K;
% end
% beta = (sumK + lbda*eye(size(sumK,1)))\yTrain;
% 
% zNew =[]
% for i=1:numGroups
%   zNew = [zNew ; gamma(i)*kFiles{i}.Phi'];
% end
% alpha = (zNew'*zNew) \ zNew'*zMat(:);
% 
% 
% oldF=0;
% maxIter=0;
% for i=1:numGroups
%   oldF = oldF + gamma(i)^2*kFiles{i}.K;
%   maxIter = maxIter + gamma(i)*kFiles{i}.Phi*zMat(:,i);
% end
% gIdx = oldF\maxIter;
% 
% 
% [cVec beta alpha gIdx]
% 
% sum(sum(kFiles{4}.K - kFiles{4}.Phi*kFiles{4}.Phi'))

end