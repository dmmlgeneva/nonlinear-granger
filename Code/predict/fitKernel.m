function fitKernel(dataFolder,dataFile,kType,recalculateKernels,lbdaGrid)
% FITKERNEL - fit kernel models for the full regularization path
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
%   kType - kernel type, see getKernel.m for possible values
%   lbdaGrid - cell with lambda grid
%   recalculateKernels - recalculate kernels (if not sure that the stored ones are still correct, default = 0)
% 
% EXAMPLE: fitKernel('N_ma','zC5_30_500',1)
%
% CREATED: MG - 11/8/2016
% MODIFICATION HISTORY:
%   MG - 04/09/2016: moved kernels to a different mat file in expData

%% fill in optional arguments
if ~exist('recalculateKernels','var') || isempty(recalculateKernels),
  recalculateKernels = 1;
end
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = num2cell(logspace(-3,4,15));
end

%% load/calculate kernels
if recalculateKernels ~= 1 && exist(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat'], 'file') == 2
  load(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat'],'K','KTest')
else
  mFile = matfile(['expData/',dataFolder,'/',dataFile,'.mat']);
  [K,Phi] = getKernel(kType,mFile.xTrain);
  KTest = getKernel(kType,mFile.xTrain,mFile.xTest);
  save(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat'],'K','KTest','Phi')
end

%% load the output yTrain
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain')

% prepare for saving data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)

%% get the whole regularization path
trainLength = size(yTrain,1);
% adapt lambda grid to the size of the problem
lbdaGrid = cellfun(@(x) x*sqrt(trainLength),lbdaGrid,'UniformOutput',0);
for lbdIdx = 1:length(lbdaGrid)
  lbda = lbdaGrid{lbdIdx};  
  Theta = (K + lbda*eye(trainLength))\yTrain;
  predTrain = K'*Theta;
  predTest = KTest'*Theta; % note the transpose here!
  save(['expResults/',dataFolder,'/',dataFile,'/Result_Kernel',num2str(kType,'%02.0f'),'_',num2str(lbdIdx,'%02.0f'),'.mat'],'Theta','predTrain','predTest','lbda')
  fprintf('fitKernel %s \n',['expResults/',dataFolder,'/',dataFile,'/Result_Kernel',num2str(kType,'%02.0f'),'_',num2str(lbdIdx,'%02.0f'),'.mat']) 
end

end
