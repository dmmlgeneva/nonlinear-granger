function [cVec,gamma,objGamma,objTotal] = MKLAlternateAlgoGroup(yTrain,KCell,cVec,gamma,lbda,updateTresh,maxIter)
% MKLALTERNATEALGOGROUP - alternating minimisation algo for MKL
%
% INPUTS
%   yTrain - output data
%   KCell - cell with multiple kernels
%   cVec, gamma - initial values of the MKL function
%   lbda - lambda hyper parameter
%   updateTresh - if change in objective smaller than this, stop grad descent (more compliated, check code)
%   maxIter - max number of iterations
%
% OUTPUTS
%   cVec, gamma - parameters of the MKL functions
% 
% EXAMPLE: MKLAlternate('N_ma','zC5_30_500',1)
%
% CREATED: MG - 4/10/2016

%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-03;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 1000;
end


%% alternating optmisation for c and gamma
trainLength = length(yTrain);
% function definitions
objectiveFunc = @(c,g) sumSquared(yTrain - bigK(KCell,g(:))*c) + lbda*c'*bigK(KCell,g(:))*c + l21Norm(g);
% initiate grad descent moitoring
objTotal = 0;
objTotal(1,1) = objectiveFunc(cVec,gamma);
while 1
  % 1) for fixed gamma get c (has a closed form solution)
  cVec = ( bigK(KCell,gamma(:)) + lbda*eye(trainLength) ) \ yTrain;

  % 2) for fixed c get gamma
  Kc = cellfun(@(x) x*cVec,KCell,'UniformOutput',0);
  Z = [Kc{:}];
  % do proximal gradient descent
  % function definitions
  %fFunc = @(g) sumSquared(yTrain-Z*g) + lbda*cVec'*Z*g; 
  %gradFunc = @(g) -2*Z'*(yTrain-Z*g) + lbda*Z'*cVec;
  fFunc = @(Zg) sumSquared(yTrain-Zg) + lbda*cVec'*Zg; 
  gradFunc = @(Zg) -2*Z'*(yTrain-Zg) + lbda*Z'*cVec;
  proxFunc = @(x,alp)  l21Prox(x,alp,1); % only positive part of shrank x;
  %objFunc = @(g) fFunc(g) + norm(g,1);
  % step size parameters
  alpha = 100; beta = 0.5; 
  % evaluate f
  Zgamma = Z*gamma(:); % (y - Phi*z)
  oldF = fFunc(Zgamma);
  % initiate grad descent moitoring
  objGamma = 0;
  objGamma(1,1) = oldF+l21Norm(gamma);
  while 1
    % evaluate grad f
    gradF = gradFunc(Zgamma);
    % gradient step with ISTA line search (BackTaboulle2010)
    while alpha>1e-5
      gammaNew = proxFunc(gamma - reshape(alpha*gradF,size(gamma)),alpha);
      ZgammaNew = Z*gammaNew(:);
      newF = fFunc(ZgammaNew);
      if newF <= oldF + gradF'*(gammaNew(:)-gamma(:)) + 0.5/alpha*sumSquared(gammaNew(:)-gamma(:))
        break
      else
        alpha = alpha*beta;
      end
    end
    gamma = gammaNew; Zgamma = ZgammaNew; oldF = newF;
    % check convergence of prox grad descent
    objGamma = [objGamma; oldF+l21Norm(gamma)];
    if length(objGamma)>5 && sum(objGamma(end-5:end-1)-objGamma(end)) < updateTresh
      break
    %elseif length(objGamma)>maxIter/10 % or check maxIter
    elseif length(objGamma)>maxIter/10 % or check maxIter
      break
    end
  end
  
  % check convergence of total grad descent
  objTotal = [objTotal ; objGamma(end)];
  if length(objTotal)>5 && sum(objTotal(end-5:end-1)-objTotal(end)) < updateTresh
    break
  elseif length(objTotal)>maxIter % or check maxIter
    break
  end
     
end

end