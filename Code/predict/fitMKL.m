function fitMKL(dataFolder,dataFile,kTypeList,recalculateKernels,lbdaGrid)
% FITMKL - fit MK model for the full regularization path using the
% alternate descend algo - this seesm to perform less well than fitMKLE so
% not used for final results
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
%   kTypeList - list of kernel types, see getKernel.m for possible values (Default = [1:6])
%   lbdaGrid - cell with lambda grid
%   recalculateKernels - recalculate kernels (if not sure that the stored ones are still correct, default = 0)
% 
% EXAMPLE: fitMKL('N_ma','zC5_30_500',1)
%
% CREATED: MG - 4/9/2016
% MODIFICATION HISTORY:
%   MG - 10/09/2016: moved the alternating algo into a seperate function

%% fill in optional arguments
if ~exist('kTypeList','var') || isempty(kTypeList),
  kTypeList = [1:6];
end
if ~exist('recalculateKernels','var') || isempty(recalculateKernels),
  recalculateKernels = 0;
end
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = num2cell(logspace(-3,4,15));
end


%% load data
numKs = 0; % total number of kernels;
kFiles = cell(length(kTypeList),1);
for kType = kTypeList
  numKs = numKs+1;
  %% load or calculate the kernels for all input data
  if recalculateKernels ~= 1 && exist(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat'], 'file') == 2
    kFiles{numKs} = matfile(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat']);
  else
    mFile = matfile(['expData/',dataFolder,'/',dataFile,'.mat']);
    [K,Phi] = getKernel(kType,mFile.xTrain);
    KTest = getKernel(kType,mFile.xTrain,mFile.xTest);
    save(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat'],'K','KTest','Phi')
    kFiles{numKs} = matfile(['expData/',dataFolder,'/',dataFile,'_K',num2str(kType),'.mat']);
  end
end
%% make cells out of the multiple input kernels
KCell = cellfun(@(x) x.K,kFiles,'UniformOutput',0);
KTestCell = cellfun(@(x) x.KTest,kFiles,'UniformOutput',0);

%% load the output yTrain
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain')

% prepare for saving data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)

%% get the whole regularization path
trainLength = size(yTrain,1);
gamma = ones(numKs,1)/numKs; % initiate evenly
cVec = zeros(trainLength,1);
% adapt lambda grid to the size of the problem
lbdaGrid = cellfun(@(x) x*sqrt(trainLength)*numKs,lbdaGrid,'UniformOutput',0);
% go from largest lambda to warm start gamma
for lbdIdx = [length(lbdaGrid):-1:1]
  lbda = lbdaGrid{lbdIdx};  
  %% alternating optmisation for c and gamma
  [cVec,gamma,objGamma,objTotal] = MKLAlternateAlgo(yTrain,KCell,cVec,gamma,lbda);
   
  % get the predictions and save results
  Theta = cVec;
  predTrain = bigK(KCell,gamma)'*Theta;
  predTest = bigK(KTestCell,gamma)'*Theta; % note the transpose here!
  save(['expResults/',dataFolder,'/',dataFile,'/Result_MKL','_',num2str(lbdIdx,'%02.0f'),'.mat'],'Theta','predTrain','predTest','lbda','gamma','kTypeList','objGamma','objTotal')
  fprintf('fitMKL %s \n',['expResults/',dataFolder,'/',dataFile,'/Result_MKL','_',num2str(lbdIdx,'%02.0f'),'.mat'])
  
end

end