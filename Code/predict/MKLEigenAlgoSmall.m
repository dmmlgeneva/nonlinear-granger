function [cVec,gamma,objTotal] = MKLEigenAlgoSmall(yTrain,PhiCell,cVec,gamma,lbda,updateTresh,maxIter)
% MKLEIGENALGOSMALL - eigendecomposition algo for MKL
%
% INPUTS
%   yTrain - output data
%   KCell - cell with multiple kernels
%   PhiCell - cell with multiple feature spaces
%   cVec, gamma - initial values of the MKL function
%   lbda - lambda hyper parameter
%   updateTresh - if change in objective smaller than this, stop grad descent (more compliated, check code)
%   maxIter - max number of iterations
%
% OUTPUTS
%   cVec, gamma - parameters of the MKL functions
% 
% EXAMPLE: MKLEigenAlgoSmall('N_ma','zC5_30_500',1)
%
% CREATED: MG - 12/09/2016

%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-03;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 1000;
end

%% group lasso for the feature representation
numGroups = numel(PhiCell);
allPhi = [PhiCell{:}];
gPhiTc = cellfun(@(x,y) x'*cVec*y,PhiCell,num2cell(gamma),'UniformOutput',0);
zVec = vertcat(gPhiTc{:});
%% by proximal gradient descent
% function definitions
%fFunc = @(zTemp) sumSquared(yTrain-allPhi*zTemp);
%gradFunc = @(zTemp) -2*allPhi'*(yTrain-allPhi*zTemp);
fFunc = @(yMphiZTemp) sumSquared(yMphiZTemp);
gradFunc = @(yMphiZTemp,idx) -2*allPhi'*yMphiZTemp;
proxFunc = @(zTemp,alp) reshape( l21Prox( reshape(zTemp,[],numGroups),2*sqrt(lbda)*alp),[],1);
%objFunc = @(zTemp) fFunc(zTemp) + 2*sqrt(lbda)*l21Norm(reshape(zTemp,[],numGroups)); 
% step size parameters
alpha = 100; beta = 0.5; 
% evaluate f
yMphiZ = yTrain-allPhi*zVec; % (y - Phi*z)
oldF = fFunc(yMphiZ);
% initiate grad descent moitoring
objTotal = 0;
objTotal(1,1) = oldF+2*sqrt(lbda)*l21Norm(reshape(zVec,[],numGroups));
while 1
  % evaluate f and grad f
  gradF = gradFunc(yMphiZ);
  % gradient step with ISTA line search (BackTaboulle2010)
  %fprintf('Linesearch \n');
  while alpha>1e-5
    zNew = proxFunc(zVec - alpha*gradF,alpha);
    yMphiZNew = yTrain-allPhi*zNew;
    newF = fFunc(yMphiZNew);
    %fprintf('NewF:%f alpha%12.6f \n',newF,alpha);
    if newF <= oldF + gradF'*(zNew-zVec) + 0.5/alpha*sumSquared(zNew-zVec)
      break
    else
      alpha = alpha*beta;
    end
  end
  zVec = zNew; yMphiZ = yMphiZNew; oldF = newF;
  % check convergence of prox grad descent
  objTotal = [objTotal; oldF+2*sqrt(lbda)*l21Norm(reshape(zVec,[],numGroups))];
  if length(objTotal)>5 && sum(objTotal(end-5:end-1)-objTotal(end)) < updateTresh
    break
  elseif length(objTotal)>maxIter % or check maxIter
    break
  end
end

%% recover gamma
zMat = reshape(zVec,[],numGroups);
for i=1:numGroups
  gamma(i) = sqrt(lbda)*norm(zMat(:,i),2);
end
%% linear system for cVec
gPhiT = cellfun(@(x,y) x'*y,PhiCell,num2cell(gamma),'UniformOutput',0);
longPhiT = vertcat(gPhiT{:});
cVec = longPhiT \ zVec;


end