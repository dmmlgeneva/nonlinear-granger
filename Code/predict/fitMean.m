function fitMean(dataFolder,dataFile)
% FITMEAN - fit simple Mean to data
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
% 
% EXAMPLE: fitMean('N_ma','zC5_30_500')
%
% CREATED: MG - 16/7/2016

%% load data
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain','yTest')

%% fit Mean model
Theta = mean(yTrain,1);
trainLength = size(yTrain,1);
testLength = size(yTest,1);
predTrain = repmat(Theta,trainLength,1);
predTest = repmat(Theta,testLength,1);

% save data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)
save(['expResults/',dataFolder,'/',dataFile,'/Result_Mean.mat'],'Theta','predTrain','predTest')

end