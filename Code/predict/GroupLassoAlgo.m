function [Theta,gamma,objTotal] = GroupLassoAlgo(yTy,XTy,XTX,Theta,numGroups,lbda,updateTresh,maxIter)
% GROUPLASSOALGO - gruop-lasso algo for linear model
%
% INPUTS
%   yTy - precaluclated yTrain'*yTrain 
%   XTy - precalculated XTrain'*yTrain
%   XTX - precalculated XTrain'*XTtrain
%   Theta - initial Theta value
%   numGroups - number of groups withing Theta for the group lasso (assumes consecutive)
%   lbda - lambda hyper parameter
%   updateTresh - if change in objective smaller than this, stop grad descent (more compliated, check code)
%   maxIter - max number of iterations
%
% OUTPUTS
%   Thtea, gamma - parameters of the MKL functions
%   objTotal - final obj value after the prox descent
% 
% EXAMPLE: GroupLassoAlgo(yTy,XTy,XTX,zeros(a,b),5)
%
% CREATED: MG - 1/10/2016

%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-03;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 1000;
end

%% group lasso by proximal gradient descent
% function definitions
fFunc = @(wTemp) yTy - 2*wTemp'*XTy + wTemp'*XTX*wTemp;
gradFunc = @(wTemp) -2*XTy + 2*XTX*wTemp;
proxFunc = @(wTemp,alp) reshape( l21Prox( reshape(wTemp,[],numGroups),lbda*alp) ,[],1);
% step size parameters
alpha = 100; beta = 0.5; 
% evaluate f
oldF = fFunc(Theta);
% initiate grad descent moitoring
objTotal = 0;
objTotal(1,1) = oldF+lbda*l21Norm(reshape(Theta,[],numGroups));
while 1
  % evaluate f and grad f
  gradF = gradFunc(Theta);
  % gradient step with ISTA line search (BackTaboulle2010)
  %fprintf('Linesearch \n');
  while alpha>1e-5
    tNew = proxFunc(Theta - alpha*gradF,alpha);
    newF = fFunc(tNew);
    if newF <= oldF + gradF'*(tNew-Theta) + 0.5/alpha*sumSquared(tNew-Theta)
      break
    else
      alpha = alpha*beta;
    end
  end
  Theta = tNew; oldF = newF;
  % check convergence of prox grad descent
  objTotal = [objTotal; oldF+lbda*l21Norm(reshape(Theta,[],numGroups))];
  if length(objTotal)>5 && sum(objTotal(end-5:end-1)-objTotal(end)) < updateTresh
    break
  elseif length(objTotal)>maxIter % or check maxIter
    break
  end
end

%% recover gamma
tMat = reshape(Theta,[],numGroups);
gamma = sqrt(diag(tMat'*tMat));


end