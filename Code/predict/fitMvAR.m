function fitMvAR(dataFolder,dataFoldersIn,dataFile,lbdaGrid)
% FITMVAR - fit multivariate AR model by ridge
%   output is a single series, inputs are multiple series in the In list
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFoldersIn - cell with names of the data folder with the input series (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
% 
% EXAMPLE: fitMvAR('S04_1',{'S04_2' 'S04_3'},'zC5_30_500')
%
% CREATED: MG - 24/09/2016

%% fill in optional arguments
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = num2cell(logspace(-3,4,15));
end

%% prepare list of files (for conveniece, include the out set in the list of in sets)
dataFoldersIn = sort(unique([dataFoldersIn {dataFolder}]));

%% load the output data
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain','xTrain','yTest')
[trainLength,numLags] = size(xTrain); testLength=length(yTest);
%% load the other series data
numIn = numel(dataFoldersIn);
xTrainMv=zeros(trainLength,numLags*numIn); xTestMv=zeros(testLength,numLags*numIn);
for inIdx = 1:numIn
  dataF = dataFoldersIn{inIdx};
  mFile = matfile(['expData/',dataF,'/',dataFile,'.mat']);
  idxs = (inIdx-1)*numLags+(1:numLags);
  xTrainMv(:,idxs) = mFile.xTrain;
  xTestMv(:,idxs) = mFile.xTest;
end

% prepare for saving data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)

%% get the whole regularization path
% adapt lambda grid to the size of the problem
lbdaGrid = cellfun(@(x) x*sqrt(trainLength)*numIn,lbdaGrid,'UniformOutput',0);
% precalculate a few things;
XTX = xTrainMv'*xTrainMv;
XTy = xTrainMv'*yTrain;
% go from largest lambda to warm start gamma
for lbdIdx = [length(lbdaGrid):-1:1]
  lbda = lbdaGrid{lbdIdx};  
  %% solve by ridge
  Theta = (XTX + lbda*eye(numLags*numIn))\XTy;
   
  % get the predictions and save results
  predTrain = xTrainMv*Theta;
  predTest = xTestMv*Theta;
  save(['expResults/',dataFolder,'/',dataFile,'/Result_MvAR_',num2str(lbdIdx,'%02.0f'),'.mat'],'Theta','predTrain','predTest','lbda','dataFoldersIn')
  fprintf('fitMvAR %s \n',['expResults/',dataFolder,'/',dataFile,'/Result_MvAR_',num2str(lbdIdx,'%02.0f'),'.mat']) 
end

