function fitMvMKLE(dataFolder,dataFoldersIn,dataFile,kTypeList,recalculateKernels,lbdaGrid,inMemory)
% FITMVMKLE - fit multivariate MK model for the full regularization path using eigendecomposition algo
%   optimised for large number of large kernels
%   output is a single series, inputs are multiple series in the In list
%
% INPUTS
%   dataFolder - name of the data folder with the output series (will search for it in the expData folder)
%   dataFoldersIn - cell with names of the data folder with the input series (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
%   kTypeList - list of kernel types, see getKernel.m for possible values (Default = [1:6])
%   lbdaGrid - cell with lambda grid
%   recalculateKernels - recalculate kernels (if not sure that the stored ones are still correct, default = 0)
% 
% EXAMPLE: fitMvMKLE('N_ma','zC5_30_500',1)
%
% CREATED: MG - 13/9/2016
% MODIFICATION HISTORY:
%   MG - 22/09/2016: adapted for fast eigen algo (needs to save working vars using -v6 version for speed)

%% fill in optional arguments
if ~exist('kTypeList','var') || isempty(kTypeList),
  kTypeList = [1:6];
end
if ~exist('recalculateKernels','var') || isempty(recalculateKernels),
  recalculateKernels = 0;
end
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = num2cell(logspace(-3,4,15));
end
if ~exist('inMemory','var') || isempty(inMemory),
  inMemory = 1;
end

%% prepare list of files (for conveniece, include the out set in the list of in sets)
dataFoldersIn = sort(unique([dataFoldersIn {dataFolder}]));

%% load data
numIn = numel(dataFoldersIn); % number of input series
numKs = 0; % total number of kernels;
kFiles = cell(length(kTypeList)*numIn,1);
for inIdx = 1:numIn
  dataF = dataFoldersIn{inIdx};
  for kType = kTypeList
    numKs = numKs + 1;
    %% load or calculate the kernels for all input data
    if recalculateKernels ~= 1 && exist(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat'], 'file') == 2
      kFiles{numKs} = matfile(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat']);
    else
      mFile = matfile(['expData/',dataF,'/',dataFile,'.mat']);
      [K,Phi] = getKernel(kType,mFile.xTrain);
      KTest = getKernel(kType,mFile.xTrain,mFile.xTest);
      save(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat'],'K','KTest','Phi')
      kFiles{numKs} = matfile(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat']);
    end
  end
end
%% load the output yTrain, yTest
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain')

%% decide weather do inMemory processing or I/O processing
%if size(kFiles{numKs}.K,1)^2*numKs >= 10e5
%if size(kFiles{numKs}.K,1)^2*numKs >= 1
%if ~inMemory==1 || size(kFiles{numKs}.K,1)^2*numKs >= 10e8
sizeK = size(kFiles{numKs}.K,1);
if ~inMemory==1 || sizeK^2*numKs >= 2e8
%if ~inMemory==1 || sizeK>=3000
  inMemory = 0;
  %PhiCell = cellfun(@(x) x.Phi,kFiles,'UniformOutput',0);
  %whos
%  return;
%   % precalculate and store some things used later
%   yTy = yTrain'*yTrain;
%   PhiTy = cell2mat([cellfun(@(x) [x.Phi]'*yTrain,kFiles,'UniformOutput',0)]');
%   PTPFileName = ['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLE_Temp_'];
%   % create the PTP files if they do not exist already
%   if exist([PTPFileName,num2str(numKs),'P',num2str(numKs),'.mat'], 'file') ~= 2
%     %save(['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLE_Temp.mat'],'-v7.3')
%     %tFile = matfile(['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLE_Temp.mat'],'Writable',true);
%     %tFile.PhiTPhi(sizeK,sizeK,numKs,numKs) = 0; % preallocate zeros
%     % and populate
%     %fprintf('Populating PhiTPhi \n');
%     for i=1:numKs
%       PTPLong=zeros(sizeK,sizeK*numKs/2); posIdx=0;
%       for j=1:numKs
%         fprintf('i=%d j=%d \n',i,j);
%         PTP = [kFiles{i}.Phi]'*kFiles{j}.Phi;
% %         if i <= j % for i>j this is just the PTP' so no need to store twice
% %           save([PTPFileName,num2str(i),'_',num2str(j),'.mat'],'-v6','PTP')
% %         end
%         % treatment for PTPLong
%         posIdx=posIdx+1;
%         idxs = (posIdx-1)*sizeK+(1:sizeK);
%         PTPLong(:,idxs) = PTP;
%         if mod(posIdx,numKs/2)==0
%           save([PTPFileName,num2str(i),'P',num2str(j),'.mat'],'-v6','PTPLong')
%           PTPLong=zeros(sizeK,sizeK*numKs/2); posIdx=0;
%         end
% %         % and the symmteric PTP 
% %         if i~=j
% %           PTP = PTP';
% %           save([PTPFileName,num2str(j),'_',num2str(i),'.mat'],'-v6','PTP')
% %         end
%       end 
%     end
%   end
% %     % and another variable often used in the EigenAlgoFast
% %     fprintf('Populating PhiTPhiLong \n');
% %     tFile.PhiTPhiLong(sizeK,sizeK*numKs,numKs) = 0; % preallocate zeros
% %     for ii=1:numKs
% %       for jj=1:numKs
% %         fprintf('ii=%d jj=%d \n',ii,jj);
% %         idxs = (jj-1)*sizeK+(1:sizeK);
% %         tFile.PhiTPhiLong(:,idxs,ii) = tFile.PhiTPhi(:,:,ii,jj);
% %       end
% %     end
% %     tFile.datum = datetime;

else
  PhiCell = cellfun(@(x) x.Phi,kFiles,'UniformOutput',0);
  KCell = cellfun(@(x) x.K,kFiles,'UniformOutput',0);
  KTestCell = cellfun(@(x) x.KTest,kFiles,'UniformOutput',0);
end
fprintf('Will work inMemory:%d \n',inMemory);

% prepare for saving data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)

%% get the whole regularization path
trainLength = size(yTrain,1);
gamma = ones(numKs,1)/numKs; % initiate evenly
cVec = zeros(trainLength,1);
% adapt lambda grid to the size of the problem
lbdaGrid = cellfun(@(x) x*sqrt(trainLength)*numKs,lbdaGrid,'UniformOutput',0);
% go from largest lambda to warm start gamma
for lbdIdx = [length(lbdaGrid):-1:1]
  lbda = lbdaGrid{lbdIdx};  
  %% eigenalgo
  if inMemory
    [cVec,gamma,objTotal] = MKLEigenAlgoSmall(yTrain,PhiCell,cVec,gamma,lbda);
    Theta = cVec;
    predTrain = bigK(KCell,gamma)'*Theta;
    predTest = bigK(KTestCell,gamma)'*Theta; % note the transpose here!
  else
    [cVec,gamma,objTotal] = MKLEigenAlgo(yTrain,kFiles,cVec,gamma,lbda);
    %[cVec,gamma,objTotal] = MKLEigenAlgoFast(yTy,PhiTy,kFiles,PTPFileName,cVec,gamma,lbda);
    %[cVec,gamma,objTotal] = MKLEigenAlgoSmall(yTrain,PhiCell,cVec,gamma,lbda);
    Theta = cVec;
    predTrain = bigK([],gamma,kFiles,0)'*Theta;
    predTest = bigK([],gamma,kFiles,1)'*Theta; % note the transpose here!
  end
   
  % save results
  save(['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLE_',num2str(lbdIdx,'%02.0f'),'.mat'],'Theta','predTrain','predTest','lbda','gamma','kTypeList','dataFoldersIn','objTotal')
  fprintf('fitMvMKLE %s inMemory:%d \n',['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLE_',num2str(lbdIdx,'%02.0f'),'.mat'],inMemory)
  
end
% and clean the temp file
% delete(['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLE_Temp.mat'])


end
