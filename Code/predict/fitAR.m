function fitAR(dataFolder,dataFile)
% FITAR - fit simple AR model by OLS
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
% 
% EXAMPLE: fitAR('N_ma','zC5_30_500')
%
% CREATED: MG - 16/7/2016

%% load data
load(['expData/',dataFolder,'/',dataFile,'.mat'])

%% fit AR model
Theta = xTrain\yTrain;
predTrain = xTrain*Theta;
predTest = xTest*Theta;

% save data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)
save(['expResults/',dataFolder,'/',dataFile,'/Result_AR.mat'],'Theta','predTrain','predTest')

end