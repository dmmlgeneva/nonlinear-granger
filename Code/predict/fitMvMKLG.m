function fitMvMKLG(dataFolder,dataFoldersIn,dataFile,kTypeList,recalculateKernels,lbdaGrid,inMemory)
% FITMVMKLG - fit multivariate grouped MK model for the full regularization path using eigendecomposition algo
%   optimised for large number of large kernels
%   output is a single series, inputs are multiple series in the In list
%
% INPUTS
%   dataFolder - name of the data folder with the output series (will search for it in the expData folder)
%   dataFoldersIn - cell with names of the data folder with the input series (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
%   kTypeList - list of kernel types, see getKernel.m for possible values (Default = [1:6])
%   lbdaGrid - cell with lambda grid
%   recalculateKernels - recalculate kernels (if not sure that the stored ones are still correct, default = 0)
% 
% EXAMPLE: fitMvMKLG('N_ma','zC5_30_500',1)
%
% CREATED: MG - 4/10/2016

%% fill in optional arguments
if ~exist('kTypeList','var') || isempty(kTypeList),
  kTypeList = [1:6];
end
if ~exist('recalculateKernels','var') || isempty(recalculateKernels),
  recalculateKernels = 0;
end
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = num2cell(logspace(-3,4,15));
end
if ~exist('inMemory','var') || isempty(inMemory),
  inMemory = 1;
end

%% prepare list of files (for conveniece, include the out set in the list of in sets)
dataFoldersIn = sort(unique([dataFoldersIn {dataFolder}]));

%% load data
numIn = numel(dataFoldersIn); % number of input series
numKs = 0; % total number of kernels;
kFiles = cell(length(kTypeList)*numIn,1);
for inIdx = 1:numIn
  dataF = dataFoldersIn{inIdx};
  for kType = kTypeList
    numKs = numKs + 1;
    %% load or calculate the kernels for all input data
    if recalculateKernels ~= 1 && exist(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat'], 'file') == 2
      kFiles{numKs} = matfile(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat']);
    else
      mFile = matfile(['expData/',dataF,'/',dataFile,'.mat']);
      [K,Phi] = getKernel(kType,mFile.xTrain);
      KTest = getKernel(kType,mFile.xTrain,mFile.xTest);
      save(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat'],'K','KTest','Phi')
      kFiles{numKs} = matfile(['expData/',dataF,'/',dataFile,'_K',num2str(kType),'.mat']);
    end
  end
end
%% load the output yTrain, yTest
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain')

%% make cells out of the multiple input kernels
%groupIdxs = logical(kron(eye(numIn),ones(numInG,1)));
%for gIdx = 1:numIn
%  KCell{gIdx} = cell2mat(cellfun(@(x) x.K,kFiles{groupIdx(:,gIdx)},'UniformOutput',0));
%end
KCell = cellfun(@(x) x.K,kFiles,'UniformOutput',0);
KTestCell = cellfun(@(x) x.KTest,kFiles,'UniformOutput',0);

%% load the output yTrain, yTest
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain')

% prepare for saving data
mkdir('expResults',dataFolder)
mkdir(['expResults/',dataFolder],dataFile)

%% get the whole regularization path
trainLength = size(yTrain,1);
% num elements in group
numInG = numKs/numIn;
gammaMat = ones(numInG,numIn)/numKs; % initiate evenly
cVec = zeros(trainLength,1);
% adapt lambda grid to the size of the problem
lbdaGrid = cellfun(@(x) x*sqrt(trainLength)*numIn,lbdaGrid,'UniformOutput',0);
% go from largest lambda to warm start gamma
for lbdIdx = [length(lbdaGrid):-1:1]
  lbda = lbdaGrid{lbdIdx};  
  %% alternating optmisation for c and gamma
  [cVec,gammaMat,objGamma,objTotal] = MKLAlternateAlgoGroup(yTrain,KCell,cVec,gammaMat,lbda);
   
  % get the predictions and save results
  Theta = cVec; gamma = gammaMat(:);
  predTrain = bigK(KCell,gamma)'*Theta;
  predTest = bigK(KTestCell,gamma)'*Theta; % note the transpose here!
  save(['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLG','_',num2str(lbdIdx,'%02.0f'),'.mat'],'Theta','predTrain','predTest','lbda','gamma','kTypeList','dataFoldersIn','objGamma','objTotal')
  fprintf('fitMvMKLG %s \n',['expResults/',dataFolder,'/',dataFile,'/Result_MvMKLG','_',num2str(lbdIdx,'%02.0f'),'.mat'])
  
end

end