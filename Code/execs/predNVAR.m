function predNVAR(prefix,fName,fNamesIn,numLags,testLength,trainLength,eT,numFold)
% PREDNVAR - call the fitMxMKLE (~NVAR) for a list of input files
%
% INPUTS
%   numFold - a text string with '_1' for an inner cv fold or an empty string for outside sample
%
% CREATED: MG - 27/09/2016

%% fill in optional arguments
if ~exist('numFold','var') || isempty(numFold),
  numFold = '';
end
if isdeployed
  eval(['fNamesIn = ' fNamesIn ';']);
end

% predict using MKL
fName1 = [eT,'_',fName];
tic;
  fitMxMKLE(fName1,fNamesIn,[prefix,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength),numFold])
tt = toc;
fprintf('fitMxMKLE %s %s time: %6.2f \n',fName1,[prefix,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength),numFold],tt)

end
