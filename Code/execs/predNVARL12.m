function predNVARL12(prefix,fName,fNamesIn,numLags,testLength,trainLength,eT,numFold)
% PREDNVARL12 - call the fitMvMKLE (~NVARL12) for a list of input files
%
% INPUTS
%   numFold - a text string with '_1' for an inner cv fold or an empty string for outside sample
%
% CREATED: MG - 4/10/2016


%% fill in optional arguments
if ~exist('numFold','var') || isempty(numFold),
  numFold = '';
end
if isdeployed
  eval(['fNamesIn = ' fNamesIn ';']);
end



% predict using MKL
fName1 = [eT,'_',fName];
tic;
  fitMvMKLG(fName1,fNamesIn,[prefix,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength),numFold])
tt = toc;
fprintf('fitMvMKLG %s %s time: %6.2f \n',fName1,[prefix,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength),numFold],tt)

end
