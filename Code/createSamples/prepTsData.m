function prepTsData(dataFolder,numLags,trainLengthVec,testLength)
% PREPTSDATA - transform time series data into input and output data
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   numLags - number of lags to use as inputs (default = 5)
%   trainLengthVec - vector of train length
%   testLength
% 
% EXAMPLE: prepTsData('nma1',3)
%
% CREATED: MG - 16/7/2016
% MODIFICATION HISTORY:
%   MG - 03/09/2016: corrected standardization and added stddev
%   MG - 3/10/2016: generation of samples directly here

%% fill in optional arguments
if ~exist('numLags','var') || isempty(numLags),
  numLags = 5;
end
if ~exist('trainLengthVec','var') || isempty(trainLengthVec),
  trainLengthVec = 100;
end
if ~exist('testLength','var') || isempty(testLength),
  testLength = 500;
end

%% load data
load(['expData/',dataFolder,'/rawData.mat'])

%% create input data
outData = tsData((1+numLags):end);
inData = zeros(length(tsData)-numLags,numLags);
for idxLag = 1:numLags
  inData(:,idxLag) = tsData((1+numLags-idxLag):(end-idxLag));
end
% save the full data
save(['expData/',dataFolder,'/nC',num2str(numLags),'.mat'],'inData','outData')

%% split to train-test data
for trainLength = trainLengthVec;
  % train
  endIdx = size(inData,1);
  idxTrain = endIdx-testLength-trainLength+1:endIdx-testLength;
  yTrain = outData(idxTrain,:);
  xTrain = inData(idxTrain,:);
  % test
  idxTest = endIdx-testLength+1:endIdx;
  yTest = outData(idxTest,:);
  xTest = inData(idxTest,:);
  
  % save data
  save(['expData/',dataFolder,'/nC',num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength),'.mat'],'yTrain','xTrain','yTest','xTest')
end

%% do the same but with standardized
tsMean = mean(tsData);
tsStd = std(tsData,1);
tsNorm = (tsData - tsMean)/tsStd;
outData = tsNorm((1+numLags):end);
inData = zeros(length(tsData)-numLags,numLags);
for idxLag = 1:numLags
  inData(:,idxLag) = tsNorm((1+numLags-idxLag):(end-idxLag));
end

% save the full data
save(['expData/',dataFolder,'/zC',num2str(numLags),'.mat'],'inData','outData','tsMean','tsStd')

%% split to train-test data
for trainLength = trainLengthVec;
  % train
  endIdx = size(inData,1);
  idxTrain = endIdx-testLength-trainLength+1:endIdx-testLength;
  yTrain = outData(idxTrain,:);
  xTrain = inData(idxTrain,:);
  % test
  idxTest = endIdx-testLength+1:endIdx;
  yTest = outData(idxTest,:);
  xTest = inData(idxTest,:);
  
  % save data
  save(['expData/',dataFolder,'/zC',num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength),'.mat'],'yTrain','xTrain','yTest','xTest')
end

%% do the same but standardized per sample sieze
outData = tsNorm((1+numLags):end);
inData = zeros(length(tsData)-numLags,numLags);
for idxLag = 1:numLags
  inData(:,idxLag) = tsNorm((1+numLags-idxLag):(end-idxLag));
end

% save the full data
save(['expData/',dataFolder,'/sC',num2str(numLags),'.mat'],'inData','outData','tsMean','tsStd')

%% split to train-test data
for trainLength = trainLengthVec;
  % train
  endIdx = size(inData,1);
  idxTrainTemp = endIdx-testLength-trainLength-numLags+1:endIdx-testLength;
  idxTrain = endIdx-testLength-trainLength+1:endIdx-testLength;
  tsMean = mean(outData(idxTrainTemp,:));
  tsStd = std(outData(idxTrainTemp,:),1);
  yTrain = (outData(idxTrain,:)-tsMean)/tsStd;
  xTrain = (inData(idxTrain,:)-tsMean)/tsStd;
  % test
  idxTest = endIdx-testLength+1:endIdx;
  yTest = (outData(idxTest,:)-tsMean)/tsStd;
  xTest = (inData(idxTest,:)-tsMean)/tsStd;
  
  % save data
  save(['expData/',dataFolder,'/sC',num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength),'.mat'],'yTrain','xTrain','yTest','xTest')
end

end