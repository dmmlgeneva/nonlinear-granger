function genNMAData(outName,tsLength,type,eTVec,alp)
% GENVARDATA - generate NMA data using random parameter values
%
% INPUTS
%   outName - file name for to save the generated data in
%   tsLength - number of observations to generate
%   type - type of NMA, see list below
%   eTVec - vector of error types (e.g. ['N', 'A']
%   alp - parameter of the MA process recursions
% OUTPUTS
%   saves genretad tsData into the outName file
% 
% EXAMPLE: genNMAData('rawData/sparseVAR.mat',500,1)
%
% CREATED: MG - 12/6/2016

%% fill in optional arguments
if ~exist('alp','var') || isempty(alp),
  alp = -1.5+3*rand(1); % uniform random in [-1.5,1.5];
end

%% define generating MA
if type == 0 % linear MA (possibly non-invertible)
  maFun = @(x) x(2:end) + alp*x(1:end-1);
elseif type == 1
  maFun = @(x) x(2:end) + alp*x(1:end-1).^2;
elseif type == 2
  maFun = @(x) x(3:end) + alp*x(1:end-2).*x(2:end-1);
elseif type == 3
  maFun = @(x) x(2:end) + alp*exp(x(1:end-1));
elseif type == 4
  maFun = @(x) x(2:end) + alp*sin(x(1:end-1)*pi);
elseif type == 5
  maFun = @(x) x(2:end) + alp*abs(x(1:end-1));
end

%% generate series with normal errors
if strfind(eTVec,'N')>0
  epsFullNorm = normrnd(0,1,tsLength+5,1); % normal distrib
  epsFull = epsFullNorm;
  tsFull = maFun(epsFull);
  % get the last bit of generated data and store
  tsData = tsFull(end-tsLength+1:end,:);
  epsData = epsFull(end-tsLength+1:end,:);
  % save data
  mkdir('expData',['N_',outName])
  save(['expData/N_',outName,'/rawData.mat'],'alp','type','tsData','epsData','tsFull','epsFull','maFun');
end

%% generate series with exp errors
if strfind(eTVec,'E')>0
  epsFullExp = exprnd(1,tsLength+5,1); % exponential distrib
  epsFull = epsFullExp;
  tsFull = maFun(epsFull);
  % get the last bit of generated data and store
  tsData = tsFull(end-tsLength+1:end,:);
  epsData = epsFull(end-tsLength+1:end,:);
  % save data
  mkdir('expData',['E_',outName])
  save(['expData/E_',outName,'/rawData.mat'],'alp','type','tsData','epsData','tsFull','epsFull','maFun');
end

%% generate series with sin of normal errors
if strfind(eTVec,'S')>0
  epsFullSin = sin(epsFullNorm*pi); % sin of normal
  epsFull = epsFullSin;
  tsFull = maFun(epsFull);
  % get the last bit of generated data and store
  tsData = tsFull(end-tsLength+1:end,:);
  epsData = epsFull(end-tsLength+1:end,:);
  % save data
  mkdir('expData',['S_',outName])
  save(['expData/S_',outName,'/rawData.mat'],'alp','type','tsData','epsData','tsFull','epsFull','maFun');
end

%% generate series with abs of normal errors
if strfind(eTVec,'A')>0
  epsFullAbs = abs(epsFullNorm); % abs of normal
  epsFull = epsFullAbs;
  tsFull = maFun(epsFull);
  % get the last bit of generated data and store
  tsData = tsFull(end-tsLength+1:end,:);
  epsData = epsFull(end-tsLength+1:end,:);
  mkdir('expData',['A_',outName])
  save(['expData/A_',outName,'/rawData.mat'],'alp','type','tsData','epsData','tsFull','epsFull','maFun');
end

%% generate series with abs of normal errors
if strfind(eTVec,'U')>0
  epsFullUni = rand(tsLength+5,1); % uniform distrib
  epsFull = epsFullUni;
  tsFull = maFun(epsFull);
  % get the last bit of generated data and store
  tsData = tsFull(end-tsLength+1:end,:);
  epsData = epsFull(end-tsLength+1:end,:);
  mkdir('expData',['U_',outName])
  save(['expData/U_',outName,'/rawData.mat'],'alp','type','tsData','epsData','tsFull','epsFull','maFun');
end

end



