function prepCvSamples(dataFolder,dataFile,trainLength,testLength,numFolds)
% PREPCVSAMPLES - split ts data into train data into cross validation samples
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFile - file within the dataFolder to generate the samples from (expects to find inData and outData)
%   trainLength - number of observations in the training sample (default = 100)
%   testLength - number of observations in the testing sample (default = 500)
%   numFolds - number of cross-validation folds (default = 5)
% 
% EXAMPLE: prepTrainTest('nma1',100,500)
%
% CREATED: MG - 11/8/2016

%% fill in optional arguments
if ~exist('numFolds','var') || isempty(numFolds),
  numFolds = 5;
end

%% load data
load(['expData/',dataFolder,'/',dataFile,'_',num2str(trainLength),'_',num2str(testLength),'.mat'])

%% split the train sample to k-folds
origY = yTrain;
origX = xTrain;
cvp = cvpartition(trainLength,'kfold',numFolds);
for cvI = 1:numFolds
  trainIdx = cvp.training(cvI);
  testIdx = cvp.test(cvI);
  yTrain = origY(trainIdx,:);
  xTrain = origX(trainIdx,:);
  yTest = origY(testIdx,:);
  xTest = origX(testIdx,:);
  % and save
  save(['expData/',dataFolder,'/',dataFile,'_',num2str(trainLength),'_',num2str(testLength),'_',num2str(cvI),'.mat'],'yTrain','xTrain','yTest','xTest','testIdx')
end  

end