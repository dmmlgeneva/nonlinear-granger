function importUSGSData(inName,outName,numRivers)
%IMPORTUSGSDATA Import the the rivers discharge data from USGS files
%
% INPUTS
%   inName - name of the txt file with the discharge data
%`  outName - name for the outputs
%   numRivers - number of rivers to create as individual series
% OUTPUTS
%   saves genretad tsData into a file in expData folder
% 
% EXAMPLE: importUSGSData('ConnecticutRiverDaily',5,'rcn')
%
% CREATED: MG - 6/9/2016

%% Open the text file.
fileID = fopen(['../../Data/USGS/',inName,'.txt'],'r');

%% read all lines
loopIdx=0;
while ~feof(fileID)
  currLine = fgetl(fileID);
  % if dataline, process
  if strfind(currLine,'USGS')==1
    loopIdx=loopIdx+1;
    [u(loopIdx,1) stationID(loopIdx,1) dateStr(loopIdx,1) discharge(loopIdx,1) flag(loopIdx,1)] = strread(currLine,'%s %s %s %f %s');
    %dt(loopIdx,1)=datetime(dateStr(loopIdx,1),'inputformat','yyyy-MM-dd');
  end
end

%% Close the text file.
fclose(fileID);

%% process loaded data
longData = table(stationID,dateStr,discharge);
wideData = unstack(longData,'discharge','stationID');
wideData = sortrows(wideData,'dateStr');
misVals = ismissing(wideData);
keepCols = find(~any(misVals,1));
fullData = wideData(:,keepCols);
fullArray = table2array(fullData(:,2:end));
growthRates = (fullArray(366:end,:)./fullArray(1:end-365,:)-1)*100;

% save the full data
mkdir('expData',inName)
save(['expData/',inName,'/completeTable.mat'],'wideData','fullData','growthRates');

% save individual rivers
% fill in optional arguments
if exist('numRivers')
  %[mVal,mIdx] = sort(mean(fullArray),'descend');
  st = floor(size(growthRates,2)/numRivers);
  idxs = 1:st:size(growthRates,2);
  idxs = idxs(1:numRivers);
  selTable = growthRates(:,idxs); 
else
  columbiaList = {'x12436500','x12438000','x12450700','x12462600','x12472800'}; siteList = columbiaList;
  %connecticutList = {'x01129200','x01144500','x01154500','x01155500'}; siteList = connecticutList;
  siteCell = cellfun(@(x) ~cellfun('isempty',strfind(fullData.Properties.VariableNames,x)), siteList,'un',0);
  sIdxs = sum(vertcat(siteCell{:}));
  selTable = growthRates(:,logical(sIdxs(2:end)));
  numRivers = size(selTable,2);
end

% get the first and last date of the returns
firstGrowth = wideData(2,1);
lastGrowth = wideData(end,1);

% clean outliers 

for rIdx = 1:numRivers
  mkdir('expData',[outName,'_',num2str(rIdx)]);
  tsData = clearOut(selTable(:,rIdx));
  stationID = fullData.Properties.VariableNames{rIdx+1};
  save(['expData/',outName,'_',num2str(rIdx),'/rawData.mat'],'tsData','stationID','firstGrowth','lastGrowth')
end

end
