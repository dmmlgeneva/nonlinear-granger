function genMultiNMAData(outName,numSeries,tsLength,A)
% GENMULTIVARDATA - generate multivariate NMA data using random parameter values
%
% INPUTS
%   outName - file name for to save the generated data in
%   numSeries - number of series to generate
%   tsLength - number of observations to generate
%   type - type of NMA, see list below
%   eTVec - vector of error types (e.g. ['N', 'A']
%   alp - parameters of the MA process recursions (numSeries x numSeries matrix)
% OUTPUTS
%   saves genretad tsData into the outName file
% 
% EXAMPLE: genMultiNMAData('rawData/sparseVAR.mat',5,500,1,'N')
%
% CREATED: MG - 7/9/2016
% MODIFICATION HISTORY:
%   MG - 19/09/2016: standardize the errors before using them

%% fill in optional arguments
if ~exist('A','var') || isempty(A) || numel(A) ~= numSeries^2
  A = 0.5+rand(numSeries); % uniform random in [0.5,1.5];
  sgn = 0.5*randn(numSeries);
  sgn = round(sgn);
  %sgn = randi([-1 1],numSeries);
  sgn = sgn - diag(diag(sgn)) + eye(numSeries);
  A = A.*sgn;
end

% A = [1.3147,0,0.6576,0,0;...
% 0,0.7785,0,0,0;...
% 0,-1.0469,1.4572,0,0;...
% 0,0,0.9854,1.2922,1.4340;...
% 0,-1.4649,-1.3003,0,1.1787]

function eps = genEps(tp)
  if tp==1
    eps = normrnd(0,1,tsLength+5,1); % normal distrib
  elseif tp==2
    eps = exprnd(1,tsLength+5,1); % exponential distrib
  elseif tp==3
    eps = rand(tsLength+5,1); % uniform distrib
  elseif tp==4
    eps = abs(normrnd(0,1,tsLength+5,1)); % abs of normal distrib
  elseif tp==5
    eps = sin(normrnd(0,1,tsLength+5,1)); % sin of normal distrib
  end
end

% first generate the errors
mFile=cell(numSeries,1);
epsFull = zeros(tsLength+5,numSeries);
epsType = 3;
for tsIdx=1:numSeries
  %if tsIdx==1 || tsIdx==2 || tsIdx==3
  %  epsType=2;
  %else
  %  epsType=3;
  %end
  %epsType = mod(tsIdx,5);
  %if epsType==0
  %  epsType=5;
  %end
  epsFull(:,tsIdx) = genEps(epsType);
  % standardize errors
  epsMean = mean(epsFull(:,tsIdx)); epsStd = std(epsFull(:,tsIdx),1);
  epsFull(:,tsIdx) = (epsFull(:,tsIdx) - epsMean)/epsStd;
  epsData = epsFull(end-tsLength+1:end,tsIdx);
  mkdir('expData',[outName,'_',num2str(tsIdx)])
  save(['expData/',outName,'_',num2str(tsIdx),'/rawData.mat'],'epsType','epsData')
  mFile{tsIdx} = matfile(['expData/',outName,'_',num2str(tsIdx),'/rawData.mat'],'Writable',true);
end

% generate the series
tsDataT = [epsFull(2:end,:)]' + A * [epsFull(1:end-1,:)]';
for tsIdx=1:numSeries
  mFile{tsIdx}.tsData = [tsDataT(tsIdx,end-tsLength+1:end)]';
  mFile{tsIdx}.alp = A(tsIdx,:);
end

end




