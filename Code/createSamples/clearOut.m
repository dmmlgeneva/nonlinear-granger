function outVar = clearOut(inVar)
% CLEAROUT  - clear outliers - replace by median of 5 preceding points if further than 6iqr from median
% MG CREATED 20/1/2016 based on Stock&Watson code

% get median and interquartile range
    p50 = nanmedian(inVar);
    pIqr = iqr(inVar);
    % compare values with median
    tmpVar = abs(inVar - p50);
    chgVar = (tmpVar > 6*pIqr);
    kpVar = (tmpVar <= 6*pIqr);
    % get the rolling medians
    rolVar = inVar;
    for i=2:length(inVar)
      rolVar(i) = nanmedian(inVar(max(1,i-5):max(1,i-1)));
    end
    % and replace outliers with rolling medians
    outVar = kpVar.*inVar + chgVar.*rolVar;
end
