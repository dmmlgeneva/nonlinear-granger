%% MG 12/6/2016 - working code to run various functions

%% setup
clear
warning('off','MATLAB:MKDIR:DirectoryExists')
%rName = 'rcc';
rName = 'xme';
%prefix = 'sC'
prefix = 'zC'
numLags = 5;
testLength = 500;
%trainLengthVec = [50 100 300 700 1000 1500 2000 3000];
trainLengthVec = [300 700 1000 1500 2000 3000];
numFolds = 5;
%numSeries = 9; % number of largest rivers to work with
numSeries = 5; % number of largest rivers to work with

tsNames = cell(0,0);
for rIdx = 1:numSeries;
%% print summary results for one data type across all series and sizes
  fName1 = [rName,'_',num2str(rIdx)];
  tsNames = [tsNames,{fName1}];
end
listNames = cell(0,0);
for trainLength = trainLengthVec;
  fName2 = [prefix,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength)];
  listNames = [listNames,{fName2}];
end
printOverallResults(tsNames,listNames,[rName,'_resultsOverall_Lag',num2str(numLags)])
fprintf('printOvearallResults %s \n',['ResultsOverall_Lag',num2str(numLags)])

