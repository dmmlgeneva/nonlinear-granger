%% MG 12/6/2016 - working code to run various functions

%% setup
warning('off','MATLAB:MKDIR:DirectoryExists')
rName = 'xme';
numLags = 5;
testLength = 500;
trainLengthVec = [50 100 300 700 1000 1500 2000 3000];
%trainLengthVec = [50 100 300 700 1000 1500 2000];
numFolds = 5;
numSeries = 5;
pref = 'zC';

%% print results for each series
for rIdx = 1:numSeries;
%% print results for each training size
  fName1 = [rName,'_',num2str(rIdx)];
  % all train and test sample sizes
  for trainLength = trainLengthVec;
    printResults(fName1,[pref,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength)])
    fprintf('printResults %s %s \n',fName1,[pref,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength)])
  end   

  % all train and test sample sizes
  for trainLength = trainLengthVec;
    plotRegPath(fName1,[pref,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength)])
    fprintf('plotRegPath %s %s \n',fName1,[pref,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength)])
  end   

%% print summary results for one data type across train sizes
  listNames = cell(0,0);
  for trainLength = trainLengthVec;
    fName2 = [fName1,'/',pref,num2str(numLags),'_',num2str(trainLength),'_',num2str(testLength)];
    listNames = [listNames,{fName2}];
  end   
  printSummaryResults(listNames,[fName1,'/',pref,'_ResultsSummary_Lag',num2str(numLags)])
  fprintf('printSummaryResults %s \n',[pref,'_ResultsSummary_Lag',num2str(numLags)])
end


