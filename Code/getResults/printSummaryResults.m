function printSummaryResults(dataFiles,outName,cvR)
% PRINTSUMMARYRESULTS - prints summary of results for multiple experiments
%
% INPUTS
%   dataFiles - cell with list of data file names to use (will search for it in the expData foler)
%   outName - suffix for the output file name
%   cvR - include results from cross-validation (default = 1)
% 
% EXAMPLE: printSummaryResults({'nma1','nma2'},'ResultsSummary_ALL')
%
% CREATED: MG - 08/08/2016
% MODIFICATION HISTORY:
%   MG - 04/09/2016: added cross-validation evaluation
%   MG - 05/09/2016: added treatment of MKL (same as for Kernel)
%   MG - 10/09/2016: added treatment of MvKernel and MvMKL (same as for Kernel)
%   MG - 14/09/2016: multiple plots if too many methods
%   MG - 24/09/2016: can load mse results if stored previously by printResults

%% fill in optional arguments
if ~exist('cvR','var') || isempty(cvR),
  cvR = 1;
end

%% list of methods to process
%listMethods = {'_AR' '_Mean' '_MvAR' '_MvLAR' '_MvKernelE01' '_MxMKLE' '_MvMKLE' '_MvMKLG'};
%listMethods = {'_AR' '_Mean' '_MvAR' '_MvLAR' '_MvMKLG' '_MxMKLE'};
listMethods = {'_AR' '_Mean' '_MvAR' '_MvLAR' '_MvMKLE' '_MvMKLG' '_MxMKLE'};

%% initialize printing
outFile = ['expPrints/',outName,'.csv'];
fileID = fopen(outFile, 'w');
outFile1 = ['expPrints/',outName,'_Short.csv'];
fileID1 = fopen(outFile1, 'w');
outFileCv = ['expPrints/',outName,'_Short_CV.csv'];
fileIDCv = fopen(outFileCv, 'w');
%% print heading
fprintf(fileID,'%s %s \n %s \n','Summary results', datestr(now,'dd/mm/yyyy HH:MM:SS'), outName);
fprintf(fileID1,'%s %s \n %s \n','Summary results', datestr(now,'dd/mm/yyyy HH:MM:SS'), outName);
if cvR
  fprintf(fileIDCv,'%s %s \n %s \n','Summary results', datestr(now,'dd/mm/yyyy HH:MM:SS'), outName);
end
  
fileCount = 0;
mseForPlot = [];
mseForPlotCv = [];
ticksForPlot = [];
dataNameForPlot = dataFiles{1}(1:strfind(dataFiles{1},'/')-1);
for mName = dataFiles
  fileCount = fileCount + 1;
  dataFile = mName{:};
  % get the sample size
  foldName = dataFile(strfind(dataFile,'/'):end);
  sizePos = strfind(foldName,'_');
  sampleSize = str2num(foldName(sizePos(1)+1:sizePos(2)-1));
  ticksForPlot = [ticksForPlot sampleSize];
  
  %% assumes data have been already calculated by printResults
  mseLoad = 1;
  load(['expPrints/',dataFile,'/Results.mat'],'metNames','mseTest','avgStdTest','testLength','confLevel');
  numMethods = numel(metNames);
   
  %% process results
  metNames1 = cell(0,0);
  metNamesCv = cell(0,0);
  for idxMet = 1:numMethods
    metName = metNames{idxMet,1};
    resFile = ['Result_',metName,'.mat'];
    %% then treat results of same kernel type for multiple lambdas as same method (the _short.csv)
    isMvKernelE = strfind(metName,'MvKernelE');
    isMvKernel = strfind(metName,'MvKernel');
    isKernel = strfind(metName,'Kernel');
    isMvMKL = strfind(metName,'MvMKL');
    isMvMKLE = strfind(metName,'MvMKLE');
    isMvMKLG = strfind(metName,'MvMKLG');
    isMxMKLE = strfind(metName,'MxMKLE');
    isMvMKLS = strfind(metName,'MvMKLS');
    isMKL = strfind(metName,'MKL');
    isMKLE = strfind(metName,'MKLE');
    isMKLS = strfind(metName,'MKLS');
    isMvAR = strfind(metName,'MvAR');
    isMvLAR = strfind(metName,'MvLAR');
    % check if any of the kernel methods (MvKernel needs to preceed Kernel cause the names overlap)
    if isMvKernelE > 0 % kernel methods have more complicated naming convention e.g. MvKernelxx_ll, this will keep only Kernelxx
      endName = isMvKernelE+10;
      metName = metName(1:endName);
    elseif isMvKernel > 0 % kernel methods have more complicated naming convention e.g. MvKernelxx_ll, this will keep only Kernelxx
      continue;
      endName = isMvKernel+9;
      metName = metName(1:endName);
    elseif isKernel > 0 % kernel methods have more complicated naming convention e.g. Kernelxx_ll, this will keep only Kernelxx
      continue;
      endName = isKernel+7;
      metName = metName(1:endName);
    elseif isMvMKLE > 0 % kernel methods have more complicated naming convention e.g. MvMKLExx_ll, this will keep only MKLxx
      endName = isMvMKLE+5;
      metName = metName(1:endName);
    elseif isMvMKLG > 0 % kernel methods have more complicated naming convention e.g. MvMKLExx_ll, this will keep only MKLxx
      endName = isMvMKLG+5;
      metName = metName(1:endName);
    elseif isMxMKLE > 0 % kernel methods have more complicated naming convention e.g. MvMKLExx_ll, this will keep only MKLxx
      endName = isMxMKLE+5;
      metName = metName(1:endName);
    elseif isMvMKLS > 0 % kernel methods have more complicated naming convention e.g. MvMKLExx_ll, this will keep only MKLxx    
      continue;
      endName = isMvMKLS+5;
      metName = metName(1:endName);
    elseif isMvMKL > 0 % kernel methods have more complicated naming convention e.g. MvMKLxx_ll, this will keep only MKLxx
      continue;
      endName = isMvMKL+4;
      metName = metName(1:endName);
    elseif isMKLS > 0 % kernel methods have more complicated naming convention e.g. MKLxx_ll, this will keep only MKLxx
      continue;
      endName = isMKLS+3;
      metName = metName(1:endName);
    elseif isMKLE > 0 % kernel methods have more complicated naming convention e.g. MKLxx_ll, this will keep only MKLxx
      continue;
      endName = isMKLE+3;
      metName = metName(1:endName);
    elseif isMKL > 0 % kernel methods have more complicated naming convention e.g. MKLxx_ll, this will keep only MKLxx
      continue;
      endName = isMKL+2;
      metName = metName(1:endName);
    elseif isMvAR > 0 % MvAR methods have more complicated naming convention e.g. MvAR_ll, this will keep only MvAR
      endName = isMvAR+3;
      metName = metName(1:endName);
    elseif isMvLAR > 0 % MvAR methods have more complicated naming convention e.g. MvAR_ll, this will keep only MvAR
      endName = isMvLAR+4;
      metName = metName(1:endName);
    end
    if sum(sum(strcmp(metNames1,metName))) > 0 % process same kernel types all together
      idxMet1 = find(strcmp(metNames1(:,1),metName)); % same row as other results of the same kernel type   
      idxCol1 = max(find(~cellfun(@isempty,metNames1(idxMet1,:))))+1; % another column for each new _ll
    else
      idxMet1 = size(metNames1,1)+1; % or new row
      idxCol1 = 1; % and first column
    end
    metNames1{idxMet1,idxCol1} = metName; % accumulate names into 2d cell (rows have same kernel, columns different lambdas)
    mseTest1{idxMet1,idxCol1} = mseTest{idxMet,1}; % accumulate mse into 2d cell (rows have same kernel, columns different lambdas)
    avgStdTest1{idxMet1,idxCol1} = avgStdTest{idxMet,1}; % accumulate mse into 2d cell (rows have same kernel, columns different lambdas)
    %% finally get the mse from cross-validation (if requested)
    if cvR && (~isempty(isKernel) || ~isempty(isMKL) || ~isempty(isMvKernel) || ~isempty(isMvMKL) || ~isempty(isMKLE) || ~isempty(isMvMKLE) || ~isempty(isMvKernelE) || ~isempty(isMvAR) || ~isempty(isMxMKLE) || ~isempty(isMvLAR) || ~isempty(isMvMKLG))
      mseCv = getCvResults(dataFile,resFile);
    else 
      mseCv = mseTest{idxMet,1};
    end
    mseTestCv{idxMet1,idxCol1} = mseCv;
  end

  %% for short summary get just the best mse per method (across multiple lambdas)
  misTest1 = cellfun(@isempty, mseTest1);
  mseTest1(find(misTest1)) = {1000}; % fill in empty cell elements with something big (AR, Mean do not have multiple lambdas)
  mseTest1Mat = cell2mat(mseTest1);
  [mVal,mIdx] = min(mseTest1Mat,[],2);
  mseForPlot = [mseForPlot mVal];
  mseTest1 = num2cell(mVal);
  % and corresponding std
  avgStdTest1(find(cellfun(@isempty, avgStdTest1))) = {0};
  avgStdTest1Mat = cell2mat(avgStdTest1);
  idx = sub2ind(size(avgStdTest1Mat), 1:size(avgStdTest1Mat,1), mIdx');
  avgStdTest1 = num2cell([avgStdTest1Mat(idx)]');

  %% for summary with cross-validated results get just the mse per best lambda in cv
  % get the best lambda from cross-validation
  mseTestCv(find(cellfun(@isempty, mseTestCv))) = {1000}; % fill in empty cell elements with something big (AR, Mean do not have multiple lambdas)
  mseTestCvMat = cell2mat(mseTestCv);
  [~,mIdxCv] = min(mseTestCvMat,[],2);
  % but pick the mse to plot from the final test results
  idxCv = sub2ind(size(mseTestCv), 1:size(mseTestCv,1), mIdxCv');
  mValCv = [mseTest1Mat(idxCv)]';
  mseForPlotCv = [mseForPlotCv mValCv];
  mseTestCv = num2cell(mValCv);
  % and corresponding std (use the same StdTable, just pick different elements)
  %idxCv = sub2ind(size(avgStdTest1Mat), 1:size(avgStdTest1Mat,1), mIdxCv');
  avgStdTestCv = num2cell([avgStdTest1Mat(idxCv)]');
  
  %% student distribu critical values
  if mseLoad~=1
    confLevel = 0.95; %confLevel = 1-alpha
  end
  tCriticValueTest = tinv((1+confLevel)/2,testLength-1);
  confIntTest = cellfun(@(x,y) x + tCriticValueTest*y,mseTest,avgStdTest,'un',0);
  confIntTest1 = cellfun(@(x,y) x + tCriticValueTest*y,mseTest1,avgStdTest1,'un',0);
  confIntTestCv = cellfun(@(x,y) x + tCriticValueTest*y,mseTestCv,avgStdTestCv,'un',0);

  % setup printing formats
  vecS = repmat([', %s'],1,numMethods);
  vecF = repmat([', %f'],1,numMethods);
  vecD = repmat([', %d'],1,numMethods);
  vecS1 = repmat([', %s'],1,size(metNames1,1));
  vecF1 = repmat([', %f'],1,size(metNames1,1));
  vecD1 = repmat([', %d'],1,size(metNames1,1));

  %% do the printing
  if fileCount == 1 % do the headings
    fprintf(fileID,['%s', vecS, '\t %s', vecS, '\n'],'MSE (test)',metNames{:},'CI MSE + t(alpha=0.05)* std(SE)/sqrt(n)',metNames{:});
    fprintf(fileID1,['%s', vecS1, '\t %s', vecS1, '\t %s', vecS1, '\n'],'MSE (test)',metNames1{:,1},'CI MSE + t(alpha=0.05)* std(SE)/sqrt(n)',metNames1{:,1},'best lbda idx',metNames1{:,1});
    if cvR
      fprintf(fileIDCv,['%s', vecS1, '\t %s', vecS1, '\t %s', vecS1, '\n'],'MSE cross-validated (test)',metNames1{:,1},'CI MSE + t(alpha=0.05)* std(SE)/sqrt(n)',metNames1{:,1},'best lbda idx',metNames1{:,1});
    end
  end
  % print out the results
  fprintf(fileID,['%s', vecF, '\t %s', vecF, '\n'],dataFile,mseTest{:},dataFile,confIntTest{:});
  fprintf(fileID1,['%s', vecF1, '\t %s', vecF1, '\t %s', vecF1, '\n'],dataFile,mseTest1{:},dataFile,confIntTest1{:},dataFile,mIdx');
  if cvR
    fprintf(fileIDCv,['%s', vecF1, '\t %s', vecF1, '\t %s', vecF1, '\n'],dataFile,mseTestCv{:},dataFile,confIntTestCv{:},dataFile,mIdxCv');
  end

  %% and save the results into a mat file
  save(['expPrints/',dataFile,'/Results.mat'],'-append','mseTestCvMat','metNames1','mseTest1','avgStdTest1','confIntTest1','mseTestCv','avgStdTestCv','confIntTestCv','mIdx','mIdxCv')

  
end

fclose(fileID);
fclose(fileID1);
if cvR
  fclose(fileIDCv);
end



%% and do plotting (do it here not to duplicate the MSE gathering
% separate ar and mean from the others
armeanMseMat = mseForPlot(logical(misTest1(:,2)),:);
othersMseMat = mseForPlot(~logical(misTest1(:,2)),:);
armeanNames = metNames1(logical(misTest1(:,2)),1);
othersNames = metNames1(~logical(misTest1(:,2)),1);

%% make multiple plots if two many lines
numSeries = size(othersMseMat,1);
serPerPlot = 8;
numPlots = ceil(numSeries/serPerPlot); % typically there are 6 kernel types plus mkl and mkle

for pIdx = 1:numPlots
  % list of series to include
  serIdx = (pIdx-1)*serPerPlot+(1:serPerPlot);
  % make sure the list is not longer than availble series
  serIdx(find(serIdx>numSeries))=[];
  % create the plot specific source data
  mseForPlot = [armeanMseMat ; othersMseMat(serIdx,:)];
  metNames1 = [armeanNames; othersNames(serIdx,1)];
  % do the plot
  msePlot = figure('Visible','Off');
  plot(mseForPlot','LineWidth',1.1)
  ylabel('MSE','FontName','Helvetica'); xlabel('Training size','FontName','Helvetica');
  xlim([0,fileCount+1]); 
  set(gca, 'XTick', [1:fileCount]');
  set(gca, 'XTickLabel', ticksForPlot,'FontSize',5,'FontName','Helvetica'); 
  title([num2str(pIdx),': MSE vs training size ',dataNameForPlot], 'interpreter', 'none','FontSize',7,'FontName','Helvetica');
  %qt = quantile(mseTestMat(:),10);
  %ylim([min(mseTestMat(:))-0.05,qt(10)]);
  leg = legend(metNames1(:,1));
  set(leg,'Location','southoutside','Orientation','horizontal');
  % anotate lines
  [~,iS] = sort(mseForPlot(:,1));
  for aIdx = 1:2:size(metNames1,1)
    tt = [metNames1(iS(aIdx),1),'\rightarrow '];
    tt1 = [tt{:}];
    text(1,mseForPlot(iS(aIdx),1),tt1,'HorizontalAlignment','right','FontName','Helvetica');
  end
  for aIdx = 2:2:size(metNames1,1)
    tt = ['\leftarrow ',metNames1(iS(aIdx),1)];
    tt1 = [tt{:}];
    text(fileCount,mseForPlot(iS(aIdx),end),tt1,'HorizontalAlignment','left','FontName','Helvetica');
  end

  set(msePlot,'PaperUnits','inches','PaperPosition',[0 0 12 6]);
  print(msePlot,'-dpng',['expPrints/',outName,'_Plot_',num2str(pIdx)]);
  close(msePlot);
end

%% finally plotting of the cross-validation if requested (do it here not to duplicate the MSE gathering
% separate ar and mean from the others
armeanMseMat = mseForPlotCv(logical(misTest1(:,2)),:);
othersMseMat = mseForPlotCv(~logical(misTest1(:,2)),:);

%% make multiple plots if two many lines
numSeries = size(othersMseMat,1);
serPerPlot = 8;
numPlots = ceil(numSeries/serPerPlot); % typically there are 6 kernel types plus mkl and mkle

for pIdx = 1:numPlots
  % list of series to include
  serIdx = (pIdx-1)*serPerPlot+(1:serPerPlot);
  % make sure the list is not longer than availble series
  serIdx(find(serIdx>numSeries))=[];
  % create the plot specific source data
  mseForPlotCv = [armeanMseMat ; othersMseMat(serIdx,:)];
  metNames1 = [armeanNames; othersNames(serIdx,1)];
  % do the plot
  msePlotCv = figure('Visible','Off');
  plot(mseForPlotCv','LineWidth',1.1)
  ylabel('MSE','FontName','Helvetica'); xlabel('Training size','FontName','Helvetica');
  xlim([0,fileCount+1]); 
  set(gca, 'XTick', [1:fileCount]');
  set(gca, 'XTickLabel', ticksForPlot,'FontSize',5); 
  title([num2str(pIdx),': MSE cross-validated vs training size ',dataNameForPlot], 'interpreter', 'none','FontSize',7,'FontName','Helvetica');
  %qt = quantile(mseTestMat(:),10);
  %ylim([min(mseTestMat(:))-0.05,qt(10)]);
  leg = legend(metNames1(:,1));
  set(leg,'Location','southoutside','Orientation','horizontal');
  % anotate lines
  [~,iS] = sort(mseForPlotCv(:,1));
  for aIdx = 1:2:size(metNames1,1)
    tt = [metNames1(iS(aIdx),1),'\rightarrow '];
    tt1 = [tt{:}];
    text(1,mseForPlotCv(iS(aIdx),1),tt1,'HorizontalAlignment','right','FontName','Helvetica');
  end
  for aIdx = 2:2:size(metNames1,1)
    tt = ['\leftarrow ',metNames1(iS(aIdx),1)];
    tt1 = [tt{:}];
    text(fileCount,mseForPlotCv(iS(aIdx),end),tt1,'HorizontalAlignment','left','FontName','Helvetica');
  end

  set(msePlotCv,'PaperUnits','inches','PaperPosition',[0 0 12 6]);
  print(msePlotCv,'-dpng',['expPrints/',outName,'_Plot_CV_',num2str(pIdx)]);
  close(msePlotCv);
  
end


