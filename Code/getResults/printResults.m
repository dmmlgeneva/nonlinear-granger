function printResults(dataFolder,dataFile)
% PRINTRESULTS - prints summary of results
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData folder)
%   dataFile - file within the dataFolder to use (expects to find yTrain ... xTest)
% 
% EXAMPLE: printResults('E_ma','cZ5_30_500')
%
% CREATED: MG - 16/07/2016
% MODIFICATION HISTORY:
%   MG - 24/09/2016: added saving result to file

%% list of methods to process
%listMethods = {'_AR' '_Mean' '_MvAR' '_MvLAR' '_MvKernelE01' '_MxMKLE' '_MvMKLE' '_MvMKLG'};
%listMethods = {'_AR' '_Mean' '_MvAR' '_MvLAR' '_MvMKLG' '_MxMKLE'};
listMethods = {'_AR' '_Mean' '_MvAR' '_MvLAR' '_MvMKLE' '_MvMKLG' '_MxMKLE'};

%% load true data
load(['expData/',dataFolder,'/rawData.mat'],'alp')
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTrain','yTest')

%% get list of all methods
fileList = dir(fullfile(['expResults/',dataFolder,'/',dataFile],'Result_*'));
fileList = {fileList.name}';
% getIndexes of fileNames without Temp
notTemp = cellfun(@(x) isempty(strfind(x,'Temp')),fileList,'UniformOutput',false);
fileList = fileList([notTemp{:}]);
numFiles = numel(fileList);
numMetods = numel(listMethods);

%% process results
%gammas = repmat({nan(1)},1,numMetods);
%residTest = cell(numMetods,1);
%mseTrain =  cell(numMetods,1);
%mseTest =  cell(numMetods,1);
%avgStdTrain = cell(numMetods,1);
%avgStdTest = cell(numMetods,1);
idxMet = 0;
for idxFile = 1:numFiles
  resFile = fileList{idxFile};
  endName = strfind(resFile,'.mat')-1;
  if sum(cell2mat(cellfun(@(x) ~isempty(strfind(resFile,x)),listMethods,'UniformOutput',false))) == 0
    continue;
  end
  %fprintf([resFile, '\n']);
  idxMet = idxMet+1;
  metNames{idxMet,1} = resFile(8:endName);
  mFile = matfile(['expResults/',dataFolder,'/',dataFile,'/',resFile]);
  [mseTrain{idxMet,1},resTrain] = getError(yTrain,mFile.predTrain);
  [mseTest{idxMet,1},resTest] = getError(yTest,mFile.predTest);
  avgStdTrain{idxMet,1} = std(resTrain.*resTrain)/sqrt(length(resTrain));
  avgStdTest{idxMet,1} = std(resTest.*resTest)/sqrt(length(resTest));
  residTest{idxMet,1} = resTest;
  % get gamma if method has it
  vars = whos(mFile);
  if ismember('gamma',{vars.name})
    gammas{1,idxMet} = mFile.gamma;
  else 
    gammas{1,idxMet} = nan;
    %fprintf(['expResults/',dataFolder,'/',dataFile,'/',resFile,'\n'])
  end 
end
%% student distribu critical values
confLevel = 0.95; %confLevel = 1-alpha
tCriticValueTrain = tinv((1+confLevel)/2,length(resTrain)-1);
testLength = length(resTest);
tCriticValueTest = tinv((1+confLevel)/2,testLength-1);
confIntTrain = cellfun(@(x,y) x + tCriticValueTrain*y,mseTrain,avgStdTrain,'un',0);
confIntTest = cellfun(@(x,y) x + tCriticValueTest*y,mseTest,avgStdTest,'un',0);

%% make gammas for all
cS = cellfun(@(x) length(x),gammas);
mC = max(cS);
gammas = cellfun(@(x) padarray(x,mC-length(x), NaN,'post'), gammas, 'UniformOutput',false);
gMat = cell2mat(gammas);

%% initialize printing
%outFile = ['expResults/',dataFile,'/Results_',datestr(now,'yyyymmdd_HHMMSS'),'.csv'];
mkdir('expPrints',[dataFolder,'/',dataFile])
outFile = ['expPrints/',dataFolder,'/',dataFile,'/Results.csv'];
fileID = fopen(outFile, 'w');
% setup printing formats
vecS = repmat([', %s'],1,idxMet);
vecF = repmat([', %f'],1,idxMet);
vecD = repmat([', %d'],1,idxMet);

%% print heading
fprintf(fileID,'%s %s \n %s \n',dataFolder, datestr(now,'dd/mm/yyyy HH:MM:SS'), [dataFolder,'/',dataFile]);
% if exist('maFun','var')
%  fprintf(fileID,['MA model %s \n'], func2str(maFun));
%  fprintf(fileID,'alp = %4.2f \n \n', alp);
% end


%% do the printing
fprintf(fileID,'MSE \n');
fprintf(fileID,['%s', vecS, '\n'],'Methods',metNames{:});
fprintf(fileID,['%s', vecF, '\n'],'Train',mseTrain{:});
fprintf(fileID,['%s', vecF, '\n'],'Test',mseTest{:});

%% do the printing
fprintf(fileID,'std(SE)/sqrt(n) \n');
fprintf(fileID,['%s', vecS, '\n'],'Methods (tCricalValue)',metNames{:});
fprintf(fileID,['%s', vecF, '\n'],'Train',avgStdTrain{:});
fprintf(fileID,['%s', vecF, '\n'],'Test',avgStdTest{:});
fprintf(fileID,'confInteval = MSE +/- t(alpha=0.05)* std(SE)/sqrt(n) \n');
fprintf(fileID,['%s', vecF, '\n'],['Train (',num2str(tCriticValueTrain),')'],confIntTrain{:});
fprintf(fileID,['%s', vecF, '\n'],['Test (',num2str(tCriticValueTest),')'],confIntTest{:});

%% print gammas
fprintf(fileID,'\n Gammas \n');
for iG = 1:size(gMat,1)
  fprintf(fileID,['%d', vecF, '\n'],iG,gMat(iG,:));
end

%% close the print file
fclose(fileID);

%% and save the results into a mat file
save(['expPrints/',dataFolder,'/',dataFile,'/Results.mat'],'metNames','mseTrain','mseTest','avgStdTrain','avgStdTest','confIntTrain','confIntTest','confLevel','gMat','testLength','residTest')

end



