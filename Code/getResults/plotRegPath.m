function plotRegPath(dataFolder,dataFile)
% PLOTREGPATH - plot the regularization path
%
% INPUTS
%   dataFolder - name of the data folder to use (will search for it in the expData and expResults folder)
%   dataFile - file within the dataFolder to use (expects to find yTest)
% 
% EXAMPLE: plotRegPath('E_ma','cZ5_30_500')
%
% CREATED: MG - 16/07/2016
% MODIFICATION HISTORY:
%   MG - 07/09/2016: added treatment of MKL (same as for Kernel)
%   MG - 12/09/2016: added treatment of MKLE (same as for Kernel)
%   MG - 14/09/2016: multiple plots if too many methods


%% load true data
load(['expData/',dataFolder,'/',dataFile,'.mat'],'yTest')

%% get list of all methods
fileList = dir(fullfile(['expResults/',dataFolder,'/',dataFile],'Result_*'));
fileList = {fileList.name}';
% getIndexes of fileNames without Temp
notTemp = cellfun(@(x) isempty(strfind(x,'Temp')),fileList,'UniformOutput',false);
fileList = fileList([notTemp{:}]);
numMethods = numel(fileList);

%% process results
metNames = cell(0,0);
for idxMet = 1:numMethods
  resFile = fileList{idxMet};
  isMvKernelE = strfind(resFile,'MvKernelE');
  isMvKernel = strfind(resFile,'MvKernel');
  isKernel = strfind(resFile,'Kernel');
  isMvMKL = strfind(resFile,'MvMKL');
  isMvMKLE = strfind(resFile,'MvMKLE');
  isMxMKLE = strfind(resFile,'MxMKLE');
  isMvMKLG = strfind(resFile,'MvMKLG');
  isMvMKLS = strfind(resFile,'MvMKLS');
  isMKL = strfind(resFile,'MKL');
  isMKLE = strfind(resFile,'MKLE');
  isMKLS = strfind(resFile,'MKLS');
  isMvAR = strfind(resFile,'MvAR');
  isMvLAR = strfind(resFile,'MvLAR');
  % check if any of the kernel methods (MvKernel needs to preceed Kernel cause the names overlap)
  if isMvKernelE > 0 % kernel methods have more complicated naming convention e.g. MvKernelxx_ll, this will keep only Kernelxx
    endName = isMvKernelE+10;
  elseif isMvKernel > 0 % kernel methods have more complicated naming convention e.g. MvKernelxx_ll, this will keep only Kernelxx
    %continue;
    endName = isMvKernel+9;
  elseif isKernel > 0 % kernel methods have more complicated naming convention e.g. Kernelxx_ll, this will keep only Kernelxx
    endName = isKernel+7;
  elseif isMvMKLE > 0 % kernel methods have more complicated naming convention e.g. MvMKLxx_ll, this will keep only MKLxx
    endName = isMvMKLE+5;
  elseif isMxMKLE > 0 % kernel methods have more complicated naming convention e.g. MvMKLxx_ll, this will keep only MKLxx
    endName = isMxMKLE+5;
  elseif isMvMKLG > 0 % kernel methods have more complicated naming convention e.g. MvMKLxx_ll, this will keep only MKLxx
    endName = isMvMKLG+5;
  elseif isMvMKLS > 0 % kernel methods have more complicated naming convention e.g. MvMKLxx_ll, this will keep only MKLxx
    %continue;
    endName = isMvMKLS+5;
  elseif isMvMKL > 0 % kernel methods have more complicated naming convention e.g. MvMKLxx_ll, this will keep only MKLxx
    endName = isMvMKL+4;
  elseif isMKLE > 0 % kernel methods have more complicated naming convention e.g. MKLExx_ll, this will keep only MKLxx
    endName = isMKLE+3;
  elseif isMKLS > 0 % kernel methods have more complicated naming convention e.g. MKLExx_ll, this will keep only MKLxx
    %continue;
    endName = isMKLS+3;
  elseif isMKL > 0 % kernel methods have more complicated naming convention e.g. MKLxx_ll, this will keep only MKLxx
    endName = isMKL+2;
  elseif isMvAR > 0 % MvAR methods have more complicated naming convention e.g. MvAR_ll, this will keep only MvAR
    endName = isMvAR+3;
  elseif isMvLAR > 0 % MvLAR methods have more complicated naming convention e.g. MvLAR_ll, this will keep only MvLAR
    endName = isMvLAR+4;
  else
    endName = strfind(resFile,'.mat')-1;
  end
  metName = resFile(8:endName);
  if sum(sum(strcmp(metNames,metName))) > 0
    idxMet = find(strcmp(metNames(:,1),metName));   
    idxCol = max(find(~cellfun(@isempty,metNames(idxMet,:))))+1;
  else
    idxMet = size(metNames,1)+1;
    idxCol = 1;
  end
  metNames{idxMet,idxCol} = metName;
  mFile = matfile(['expResults/',dataFolder,'/',dataFile,'/',resFile]);
  [mseTest{idxMet,idxCol},resTest] = getError(yTest,mFile.predTest); 
end

%% and plot the results
% AR and Mean methods have no regularization path so need to replicate single values into the missing columns
misTest = cellfun(@isempty, mseTest);
mseTest(find(misTest)) = {0};
mseTestMat = cell2mat(mseTest);
repFirstCol = repmat(mseTestMat(:,1),1,size(mseTestMat,2));
mseTestMat = misTest.*repFirstCol+mseTestMat;
% separate ar and mean from the others
armeanMseMat = mseTestMat(logical(misTest(:,2)),:);
othersMseMat = mseTestMat(~logical(misTest(:,2)),:);
armeanNames = metNames(logical(misTest(:,2)),1);
othersNames = metNames(~logical(misTest(:,2)),1);

%% make multiple plots if two many lines
numSeries = size(othersMseMat,1);
serPerPlot = 9;
numPlots = ceil(numSeries/serPerPlot); % typically there are 6 kernel types plus mkl and mkle

for pIdx = 1:numPlots
  % list of series to include
  serIdx = (pIdx-1)*serPerPlot+(1:serPerPlot);
  % make sure the list is not longer than availble series
  serIdx(find(serIdx>numSeries))=[];
  % create the plot specific source data
  mseTestMat = [armeanMseMat ; othersMseMat(serIdx,:)];
  metNames = [armeanNames; othersNames(serIdx,1)];
  % do the plot
  msePlot = figure('Visible','Off');
  plot(mseTestMat','LineWidth',1.1)
  ylabel('MSE','FontName','Helvetica'); xlabel('lambda idx','FontName','Helvetica');
  title([num2str(pIdx),': Regularization path ',dataFolder,'/',dataFile], 'interpreter', 'none','FontSize',7,'FontName','Helvetica');
  qt = quantile(mseTestMat(:),10);
  ylim([min(mseTestMat(:))-0.05,qt(10)]);
  leg = legend(metNames(:,1));
  set(leg,'Location','southoutside','Orientation','horizontal');
  % anotate lines
  [~,iS] = sort(mseTestMat(:,1));
  sPoint = floor(size(mseTestMat,2)/3);
  for aIdx = 1:2:size(metNames,1)
    tt = [metNames(iS(aIdx),1),'\rightarrow '];
    tt1 = [tt{:}];
    text(sPoint,mseTestMat(iS(aIdx),sPoint),tt1,'HorizontalAlignment','right','FontName','Helvetica');
  end
  midPoint = floor(size(mseTestMat,2)/2);
  for aIdx = 2:2:size(metNames,1)
    tt = ['\leftarrow ',metNames(iS(aIdx),1)];
    tt1 = [tt{:}];
    text(midPoint,mseTestMat(iS(aIdx),midPoint),tt1,'HorizontalAlignment','left','FontName','Helvetica');
  end

  set(msePlot,'PaperUnits','inches','PaperPosition',[0 0 12 6]);
  mkdir('expPrints',[dataFolder,'/',dataFile])
  print(msePlot,'-dpng',['expPrints/',dataFolder,'/',dataFile,'/RegPath_',num2str(pIdx)]);
  close(msePlot);
end

end


