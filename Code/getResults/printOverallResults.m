function printOverallResults(listSeries,listFiles,outName)
% PRINTOVERALLRESULTS - prints overall results for all ts i dataFiles
%   This is far from neat and optimised but does the job
%
% INPUTS
%   dataFiles - cell with list of data file names to use (will search for it in the expData foler)
%   outName - suffix for the output file name
% 
% EXAMPLE: printOverallResults({'nma1','nma2'},'ResultsSummary_ALL')
%
% CREATED: MG - 24/09/2016

%% initiate some constants
fNum = numel(listFiles);
tsNum = numel(listSeries);

%% student distribu critical values
confLevel = 0.90; %confLevel = 1-alpha
testLength = 500;
tCriticValueTest = tinv((1+confLevel)/2,(tsNum*testLength)-1);

%% load all the mse data
mseTestCell = repmat({0},fNum,1);
mseTestCellCv = repmat({0},fNum,1);
mseTestCellAll = repmat({0},fNum,1);
ticksForPlot = [];
for fIdx = 1:fNum;
  mseTestBig = 0;
  mseTestCvBig = 0;
  for tsIdx = 1:tsNum;
    load(['expPrints/',listSeries{tsIdx},'/',listFiles{fIdx},'/Results.mat'], 'mseTestCvMat', 'mseTest', 'metNames1','mseTest1','avgStdTest1','testLength','mseTestCv','avgStdTestCv','mIdx','mIdxCv','metNames','gMat','residTest');
    mseTestBig = mseTestBig + cell2mat(mseTest)/tsNum;
    mseTestCvBig = mseTestCvBig + mseTestCvMat/tsNum;
    mseTestCell{fIdx,1} = mseTestCell{fIdx,1} + cell2mat(mseTest1)/tsNum;
    mseTestCellCv{fIdx,1} = mseTestCellCv{fIdx,1} + cell2mat(mseTestCv)/tsNum;
    % get the best gamma vectors
    for mI = 1:size(metNames1,1)
      mName = metNames1{mI,1};
      mets(mI,1) = min(find(cellfun(@(x) (nansum([~isempty(x),(x==1)])==2), strfind(metNames,mName))));
    end
  % watchout a temp fix here !!!!! 
    if fIdx == 5
      metsFix = mets;
      metNamesFix = metNames1;
    end
    if fIdx == 6 % cheat to get data gor mvMKLG for the largest sample as a copy of mvMKLE (the mvMKLG didn't finish running)
      mIdx = [mIdx(1:4);mIdx(5);mIdx(5:6)];
      mIdxCv = [mIdxCv(1:4);mIdxCv(5);mIdxCv(5:6)];
      gMat = [gMat(:,1:32) gMat(:,33:47) gMat(:,33:end)];
      mets = metsFix;
      metNames1 = metNamesFix;
    end
    gammaIdxs = mIdx+mets-1;
    gammaCvIdxs = mIdxCv+mets-1;
    subs{1} = reshape(repmat((1:tsNum),size(gMat,1)/tsNum,1),[],1);
    subs{2} = [[1:tsNum]'; (tsNum+1)*ones(size(gMat,1)-tsNum,1)];
    gTemp = cellfun(@(x) accumarray(subs{1 + double(sum(isnan(x))>0)},x), num2cell(gMat(:,gammaIdxs),1),'UniformOutput',false);
    gamma{fIdx,tsIdx} = cell2mat(cellfun(@(x) x(1:tsNum), gTemp, 'UniformOutput',false));
    gTemp = cellfun(@(x) accumarray(subs{1 + double(sum(isnan(x))>0)},x), num2cell(gMat(:,gammaCvIdxs),1),'UniformOutput',false);
    gammaCv{fIdx,tsIdx} = cell2mat(cellfun(@(x) x(1:tsNum), gTemp, 'UniformOutput',false));
    gammaBig{tsIdx} = gMat;
    residBig{tsIdx} = horzcat(residTest{:});
  end
  % get the sample size
  sizePos = strfind(listFiles{fIdx},'_');
  sampleSize = str2num(listFiles{fIdx}(sizePos(1)+1:sizePos(2)-1));
  ticksForPlot = [ticksForPlot sampleSize]; 

  % get the best mse with same lambda across methods
  %!!!!!!!!!!!!!!!!!!! this only will work if I have this specific structure
  mseTestBig = [repmat(mseTestBig(1:2,1),1,15) ; reshape(mseTestBig(3:end,1),15,[])'];
  [~,mIdxCv] = min(mseTestCvBig,[],2);
  % but pick the mse to plot from the final test results
  idxMse = sub2ind(size(mseTestBig), 1:size(mseTestBig,1), mIdxCv');
  mseTestCellAll{fIdx,1} = [mseTestBig(idxMse)]';
  % get corresponding gammas
  idxGamma = sub2ind(size(mseTestBig'), mIdxCv', 1:size(mseTestBig,1));
  gammaTemp = cellfun(@(x) [repmat(x(:,1),1,15) repmat(x(:,2),1,15) x(:,3:end)], gammaBig, 'UniformOutput',false);
  gAccum = @(gmat) cellfun(@(x) accumarray(subs{1 + double(sum(isnan(x))>0)},x), num2cell(gmat(:,idxGamma),1),'UniformOutput',false);
  gTemp = cellfun(@(x) gAccum(x), gammaTemp, 'UniformOutput', false);
  gSel = @(gmat) cell2mat(cellfun(@(x) x(1:tsNum), gmat, 'UniformOutput',false));
  gammaAll(fIdx,1:tsNum) = cellfun(@(x) gSel(x), gTemp, 'UniformOutput',false);
  % and corresponding residuals
  resTemp = cellfun(@(x) [repmat(x(:,1),1,15) repmat(x(:,2),1,15) x(:,3:end)], residBig, 'UniformOutput',false);
  rSel = cellfun(@(x) x(:,idxGamma).*x(:,idxGamma),resTemp,'UniformOutput',false);
  avgStdTest{fIdx,1} = std(vertcat(rSel{:}))'/sqrt(tsNum*testLength);
  confIntTest{fIdx,1} = mseTestCellAll{fIdx,1} + tCriticValueTest*avgStdTest{fIdx,1};
end




% %% initialize printing
% outFile = ['expPrints/',outName,'.csv'];
% fileID = fopen(outFile, 'w');
% %% print heading
% fprintf(fileID,'%s %s \n %s \n','Overall results', datestr(now,'dd/mm/yyyy HH:MM:SS'), outName);
% 
% % setup printing formats
% vecS1 = repmat([', %s'],1,size(metNames1,1));
% vecF1 = repmat([', %f'],1,size(metNames1,1));
% vecD1 = repmat([', %d'],1,size(metNames1,1));
% 
% %% do the printing
% fprintf(fileID,['%s', vecS1, '\t %s', vecS1, '\t %s', vecS1, '\n'],'MSE All (test)',metNames1{:,1},'MSE CV (test)',metNames1{:,1},'MSE best (test)',metNames1{:,1});
% % print out the results
% for fIdx = 1:fNum;
%   fprintf(fileID,['%s', vecF1, '\t %s', vecF1, '\t %s', vecF1, '\n'],listFiles{fIdx},mseTestCellAll{fIdx,1},listFiles{fIdx},mseTestCellCv{fIdx,1},listFiles{fIdx},mseTestCell{fIdx,1});
% end
% fprintf(fileID,['%s', vecS1, '\n'],'Avg std (test)',metNames1{:,1});
% % print out conf intervals
% for fIdx = 1:fNum;
%   fprintf(fileID,['%s', vecF1, '\n'],listFiles{fIdx},avgStdTest{fIdx,1});
% end
% fprintf(fileID,['%s', vecS1, '\n'],'Confint (test)',metNames1{:,1});
% % print out conf intervals
% for fIdx = 1:fNum;
%   fprintf(fileID,['%s', vecF1, '\n'],listFiles{fIdx},confIntTest{fIdx,1});
% end
% 
% % close the file
% fclose(fileID);
% 
% %%%%%% Plots of MSE %%%%%
% % list of methods to plot (and print)
% %listPlot = {'AR' 'Mean' 'MvAR' 'MvLAR' 'MvKernelE01' 'MxMKLE' 'MvMKLE'};
% listPlot = {'AR' 'Mean' 'MvAR' 'MvLAR' 'MvMKLG' 'MxMKLE'};
% listTitles = {'LAR' 'Mean' 'LVARL2' '  LVARL1' 'NVARL12' 'NVAR'};
listPlot = {'AR' 'Mean' 'MvAR' 'MvLAR' 'MvMKLE' 'MvMKLG' 'MxMKLE'};
listTitles = {'LAR' 'Mean' 'LVARL2' '  LVARL1' '  NVARL1' 'NVARL12' 'NVAR'};
% 
% %% do plotting
% idxs = zeros(size(metNames1,1),1);
% for mI=1:numel(listPlot)
%   idxs = idxs + cell2mat(cellfun(@(x) (nansum([~isempty(x),(x==1)])==2), strfind(metNames1(:,1),listPlot{mI}),'UniformOutput',false)); 
% end
% mseForPlot = cell2mat([cellfun(@(x) x(logical(idxs)),mseTestCell,'UniformOutput',false)]');
% mseForPlotCv = cell2mat([cellfun(@(x) x(logical(idxs)),mseTestCellCv,'UniformOutput',false)]');
% mseForPlotAll = cell2mat([cellfun(@(x) x(logical(idxs)),mseTestCellAll,'UniformOutput',false)]');
% namesForPlot = metNames1(logical(idxs),1);
% 
% % do the BEST plot
% msePlot = figure();
% plot(mseForPlot','LineWidth',1.1)
% ylabel('MSE Best','FontName','Helvetica','FontSize',9); xlabel('Training size','FontName','Helvetica','FontSize',9);
% xlim([0,fNum+1]); 
% set(gca, 'XTick', [1:fNum]');
% set(gca, 'XTickLabel', ticksForPlot,'FontSize',9,'FontName','Helvetica'); 
% title('Best MSE vs training size', 'interpreter', 'none','FontSize',11,'FontName','Helvetica');
% leg = legend(namesForPlot);
% set(leg,'Location','southoutside','Orientation','horizontal');
% % anotate lines
% [~,iS] = sort(mseForPlot(:,1));
% for aIdx = 2:2:size(namesForPlot,1)
%   tt = [namesForPlot(iS(aIdx),1),'\rightarrow '];
%   tt1 = [tt{:}];
%   text(1,mseForPlot(iS(aIdx),1),tt1,'HorizontalAlignment','right','FontName','Helvetica');
% end
% for aIdx = 1:2:size(namesForPlot,1)
%   tt = ['\leftarrow ',namesForPlot(iS(aIdx),1)];
%   tt1 = [tt{:}];
%   text(fNum,mseForPlot(iS(aIdx),end),tt1,'HorizontalAlignment','left','FontName','Helvetica');
% end
% set(msePlot,'PaperUnits','inches','PaperPosition',[0 0 12 6]);
% print(msePlot,'-dpng',['expPrints/',outName,'_BestPlot']);
% close(msePlot);
% 
% 
% 
% % do the CV plot
% msePlot = figure();
% plot(mseForPlotCv','LineWidth',1.1)
% ylabel('MSE CV','FontName','Helvetica','FontSize',9); xlabel('Training size','FontName','Helvetica','FontSize',9);
% xlim([0,fNum+1]); 
% set(gca, 'XTick', [1:fNum]');
% set(gca, 'XTickLabel', ticksForPlot,'FontSize',9,'FontName','Helvetica'); 
% title('CV MSE vs training size', 'interpreter', 'none','FontSize',11,'FontName','Helvetica');
% leg = legend(namesForPlot);
% set(leg,'Location','southoutside','Orientation','horizontal');
% % anotate lines
% [~,iS] = sort(mseForPlotCv(:,1));
% for aIdx = 2:2:size(namesForPlot,1)
%   tt = [namesForPlot(iS(aIdx),1),'\rightarrow '];
%   tt1 = [tt{:}];
%   text(1,mseForPlotCv(iS(aIdx),1),tt1,'HorizontalAlignment','right','FontName','Helvetica');
% end
% for aIdx = 1:2:size(namesForPlot,1)
%   tt = ['\leftarrow ',namesForPlot(iS(aIdx),1)];
%   tt1 = [tt{:}];
%   text(fNum,mseForPlotCv(iS(aIdx),end),tt1,'HorizontalAlignment','left','FontName','Helvetica');
% end
% set(msePlot,'PaperUnits','inches','PaperPosition',[0 0 12 6]);
% print(msePlot,'-dpng',['expPrints/',outName,'_CVPlot']);
% close(msePlot);
% 
% 
% % do the All plot
% msePlot = figure();
% plot(mseForPlotAll','LineWidth',4)
% ylabel('MSE','FontName','Helvetica','FontSize',20); xlabel('Training size','FontName','Helvetica','FontSize',20);
% xlim([0,fNum+1]); 
% set(gca, 'XTick', [1:fNum]');
% set(gca, 'XTickLabel', ticksForPlot,'FontSize',20,'FontName','Helvetica'); 
% title('MSE vs training size', 'interpreter', 'none','FontSize',25,'FontName','Helvetica');
% leg = legend(namesForPlot);
% set(leg,'Location','southoutside','Orientation','horizontal','Box','off');
% % anotate lines
% [~,iS] = sort(mseForPlotAll(:,1));
% for aIdx = 2:2:size(namesForPlot,1)
%   tt = [namesForPlot(iS(aIdx),1),'\rightarrow '];
%   tt1 = [tt{:}];
%   text(1,mseForPlotAll(iS(aIdx),1),tt1,'HorizontalAlignment','right','FontName','Helvetica','FontSize',20);
% end
% for aIdx = 1:2:size(namesForPlot,1)
%   tt = ['\leftarrow ',namesForPlot(iS(aIdx),1)];
%   tt1 = [tt{:}];
%   text(fNum,mseForPlotAll(iS(aIdx),end),tt1,'HorizontalAlignment','left','FontName','Helvetica','FontSize',20);
% end
% set(msePlot,'PaperUnits','inches','PaperPosition',[0 0 12 10]);
% print(msePlot,'-dpng',['expPrints/',outName,'_AllPlot']);
% close(msePlot);


%%%%%% Plots of gamma %%%%%
% %%V1)
% % for each sample size
% for fIdx = 1:fNum;
%   % get all gammas for the sample size (across all ts and methods)
%   gPlotAll = cat(3,gammaAll{fIdx,:});
%   % get number of methods with gamma
%   numGMeths = sum(~isnan(gPlotAll(1,:,1)));
%   % set up the printing
%   heatMaps = figure();
%   %set(heatMaps, 'Visible', 'off')
%   myMap = flipud(colormap(gray));
%   colormap(myMap)
%   brighten(-0.5)
%   plotIdx=0;
%   % for each method
%   for mI = 1:size(metNames1,1)
%     mName = metNames1{mI,1};
%     % get gamma for the methods across all ts
%     gPlot = squeeze(gPlotAll(:,mI,:));
%     % normalize
%     gPlot = gPlot / norm(gPlot,'fro');
%     %gPlot = bsxfun(@rdivide, gPlot, [sqrt(diag(gPlot'*gPlot))]');
%     % and remember that model per ts should be in a row
%     gPlot = gPlot';
%     if sum(isnan(gPlot)) == 0
%       plotIdx=plotIdx+1;
%       subplot(1,numGMeths,plotIdx);
%       imagesc([],[],gPlot,[0 1])
%       title(mName, 'interpreter', 'none','FontSize',7);
%       set(gca,'FontSize',7)
%       set(gca,'xtick',[],'ytick',[])
%     end
%   end 
%   heatMaps.PaperUnits = 'inches';
%   heatMaps.PaperPosition = [0 0 3 2];
%   % get the sample size
%   sampleSize = ticksForPlot(fIdx);
%   print(heatMaps,'-dpng',['expPrints/',outName,'_HeatCV_',num2str(sampleSize)]);
%   close(heatMaps);
% end

%%V2)
% for each method
% set up the printing
heatMaps = figure();
%set(heatMaps, 'Visible', 'off')
myMap = flipud(colormap(gray));
colormap(myMap)
brighten(-0.5)
plotIdx=0;
for mI = 1:size(metNames1,1)-1
  mName = metNames1{mI,1};
  %ax2 = axes('Position',[0.2 0 0.8 1]);
  % for each sample size
  plotInRow=0;
  for fIdx = 1:fNum; 
    % get the relevant gammas for all series
    gTemp = cat(3,gammaAll{fIdx,:});
    % all series for a single method  
    gTemp = squeeze(gTemp(:,mI,:));
    % only continue if gamma for the method exists 
    if sum(isnan(gTemp)) == 0
      % normalize
      %gTempPlot = gTemp / norm(gTemp,'fro');
      %gTempPlot = gTemp./repmat(sum(gTemp),size(gTemp,1),1)
      gTempPlot = gTemp/max(max(gTemp));
      %gTemp = bsxfun(@rdivide, gTemp, [sqrt(diag(gTemp'*gTemp))]');
      % remember that model per ts are in colums
      % do the plotting
      plotIdx=plotIdx+1;
      plotInRow = plotInRow+1;
      plotCol = floor((plotIdx-1)/fNum);
      s(plotIdx) = subplot(3,fNum,plotIdx);
      p = get(s(plotIdx),'Position')
      p(1) = p(1)-0.1+0.015*plotInRow; p(2) = p(2)-0.03-0.01*plotCol;
      %p(2) = p(2)-0.05+0.01*plotCol;
      p(3) = p(3)*1.3; p(4) = p(4)*1.1; 
      set(s(plotIdx),'Position',p);
      imagesc([],[],gTempPlot,[0 1])
      % get the sample size
      sampleSize = ticksForPlot(fIdx);
      title(num2str(sampleSize), 'interpreter', 'none','FontSize',15);
      %set(gca,'FontSize',7)
      set(gca,'xtick',[],'ytick',[])
    else
     break;
    end
  end
%  if sum(isnan(gTemp)) == 0
%     % and save
%     %axes(ax1)   
%     ax1 = axes('Position',[0 0 1 1],'Visible','off');
%     t = text(0.1,0.1,listTitles{mI},'HorizontalAlignment','left','FontName','Helvetica','FontSize',10,'Rotation',90,'FontWeight','bold');
%     heatMaps.PaperUnits = 'inches';
%     heatMaps.PaperPosition = [0 0 5 1];
%     print(heatMaps,'-dpng',['expPrints/',outName,'_Heat_',mName]);
%     close(heatMaps);
%   end
end
ax1 = axes('Position',[0 0 1 1],'Visible','off');
c = colorbar;
set(c,'Position',[0.94    0.08    0.01    0.81]) 
set(c,'FontSize',10)
t1 = text(0.02,0.70,'LVARL1','HorizontalAlignment','left','FontName','Helvetica','FontSize',15,'Rotation',90,'FontWeight','bold');
t2 = text(0.02,0.39,'NVARL1','HorizontalAlignment','left','FontName','Helvetica','FontSize',15,'Rotation',90,'FontWeight','bold');
t3 = text(0.02,0.07,'NVARL12','HorizontalAlignment','left','FontName','Helvetica','FontSize',15,'Rotation',90,'FontWeight','bold');

end

  



