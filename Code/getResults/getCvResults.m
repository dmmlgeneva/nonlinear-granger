function mseTot = getCvResults(dataFile,resFile)
% GETCVRESULTS - helper function called by printSummaryResults to get results from cross-validation
%
% INPUTS
%   dataFiles - cell with list of data file names to use (will search for it in the expData foler)
%   outName - suffix for the output file name
%   cvR - include results from cross-validation (default = 1)
% 
% EXAMPLE: getCvResults()
%
% CREATED: MG - 04/09/2016

%% get list of cv samples
fList = dir(fullfile(['expResults/',dataFile,'_*']));
fList = {fList.name}';
numFolds = numel(fList);

%% process results
mseTot = 0;
for idxFold = 1:numFolds
    % get true data of the fold
    tFile = matfile(['expData/',dataFile,'_',num2str(idxFold),'.mat']);
    % get results of the fold
    rFile = matfile(['expResults/',dataFile,'_',num2str(idxFold),'/',resFile]);
    % calculate mse
    mseTest = getError(tFile.yTest,rFile.predTest);
    % and an average across the cv samples
    mseTot = mseTot+mseTest/numFolds;
end

  
end


